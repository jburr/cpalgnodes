.. cpalgnodes documentation master file, created by
   sphinx-quickstart on Fri Aug 12 11:40:52 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
.. role:: python(code)
    :language: python

.. role:: cpp(code)
    :language: cpp

.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Overview <overview/index>
   Full API <api/index>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
