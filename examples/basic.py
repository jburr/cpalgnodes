from operator import attrgetter
from typing import Iterable, Set

import matplotlib.pyplot as plt
import networkx as nx
from cpalgnodes.drawing import GraphDrawer, GraphDrawerConfig, multipartite_layout
from cpalgnodes.graph import BaseNode, GraphBuilder


class BasicNode(BaseNode):
    """Simple node class used for examples"""

    def __init__(
        self,
        name: str,
        produces: Iterable[str] = (),
        requires: Iterable[str] = (),
        priority: int = 0,
    ):
        """Create the node"""
        self._name = name
        self._produces = set(produces)
        self._requires = set(requires)
        self._priority = priority

    @property
    def name(self) -> str:
        return self._name

    def __hash__(self) -> int:
        return hash(self.name)

    def __eq__(self, other: "BasicNode"):
        return (
            self.name == other.name
            and self.produces == other.produces
            and self.requires == other.requires
            and self.priority == other.priority
        )

    @property
    def produces(self) -> Set[str]:
        return self._produces

    @property
    def requires(self) -> Set[str]:
        return self._requires

    @property
    def priority(self) -> int:
        return self._priority


# [create-graph]
# Create the builder and add in some simple nodes
builder = GraphBuilder()
builder += [
    BasicNode("A", produces=["a", "b"]),
    BasicNode("B", produces=["c"], requires=["a"]),
    BasicNode("C", produces=["d"], requires=["b"], priority=10),
    BasicNode("D", produces=["e"], requires=["c"], priority=20),
    BasicNode("E", requires=["d", "e"]),
]

# Create the graph
graph = builder.build_graph(ignore_unmet=False)
# [end-create-graph]

# Create a layout to use in the visualisation
# layout = nx.spring_layout(graph)
layout = multipartite_layout(graph)

# Draw the nodes with their dependency relations
fig, ax = plt.subplots()
GraphDrawer(GraphDrawerConfig(draw_total_order=False)).draw_mpl(
    graph, ax=ax, layout=layout
)
fig.savefig("basic_no_order.svg")

# [order-graph]
# Calculate and annotate the execution order
order = builder.order_graph(graph)
builder.annotate_order(graph, order)
# [end-order-graph]

# Draw the nodes with the calculated execution order
fig, ax = plt.subplots()
GraphDrawer().draw_mpl(graph, ax=ax, layout=layout)
fig.savefig("basic_order.svg")

# [reorder-graph]
# Use a weak dependency to force the builder to order B before C
builder.add_weak_dependency("B", "C")
reorder = builder.order_graph(graph)
builder.annotate_order(graph, reorder)
# [end-reorder-graph]

fig, ax = plt.subplots()
GraphDrawer().draw_mpl(graph, ax=ax, layout=layout)
fig.savefig("basic_reorder.svg")
