"""Base classes for setting up the underlying graph structure

These classes describe the basic graph structure:

    BaseGraph
        The basic graph class used in the representation.
    NodeBase
        The base class for all nodes
    GraphBuilder
        The class responsible for constructing the data dependency graph and working out the
        correct execution order
"""

import abc
import collections.abc
import functools
import logging
from typing import Any, Callable, Dict, Iterable, List, Tuple, Union

import networkx

log = logging.getLogger(__name__)


class BaseGraph(networkx.DiGraph):
    """Base for custom graph classes

    The edges in this graph represent data dependencies between the nodes. The individual data
    items are then added to these edges as attributes. Additional strong dependencies not due to
    a data dependency can also be added. These are marked with an 'AdditionalStrongDependency' edge
    attribute.
    """

    @classmethod
    def edge_attr_dict_factory(cls):
        """Define attributes to appear on every edge"""
        return {"DataDependencies": set(), "AdditionalStrongDependency": False}

    @classmethod
    def _factory(cls):
        """Factory method used to create an empty graph"""
        return cls()

    def filter_edges(self, condition: Callable[["BaseNode", "BaseNode", Dict], bool]):
        """Produces a subgraph containing only edges passing the condition

        Parameters
        ----------
        condition: Callable[[BaseNode, BaseNode, Dict], bool]
            The returned subgraph will only contain edges which pass this condition. The arguments
            are the input and output nodes of the edge and its dictionary of attributes.
        """
        # Create the empty graph
        graph = self._factory()
        # Add only those edges which satisfy the condition
        graph.add_edges_from(e for e in self.edges(data=True) if condition(*e))
        # Add back all the node attributes, only for those nodes in the new graph
        networkx.set_node_attributes(graph, {node: self.nodes[node] for node in graph})
        return graph

    @property
    def data_dependency_graph(self):
        """Return a graph only containing the data dependencies"""
        # NB: The edge attributes are stored in the third member of the tuple returned by edges
        return self.filter_edges(lambda _1, _2, attrs: attrs["DataDependencies"])

    @property
    def strong_dependency_graph(self):
        """Return a graph containing the strong dependencies (including) the data dependencies"""
        return self.filter_edges(
            lambda _1, _2, attrs: attrs["DataDependencies"]
            or attrs["AdditionalStrongDependency"]
        )

    def define_edge_attribute(self, key: str, default_value: Any):
        """Define a new attribute

        Parameters
        ----------
        key: str
            The name of the attribute
        default_value
            The default value to set for the attribute. It can also be a callable, taking the
            in node, out node and edge as arguments and returning the new value to set.
        """
        for in_node, out_node, edge in self.edges(data=True):
            if callable(default_value):
                edge[key] = default_value(in_node, out_node, edge)
            else:
                edge[key] = default_value


class BaseNode(abc.ABC):
    """Abstract base class for nodes in the dependency graph

    A node is the base unit of the graph. Each node reports the data that it produces and that it consumes.
    The dependency mechanism then uses this information to calculate the order in which they should
    be executed.

    Each node should have a unique name so that it can be retrieved from the graph.

    The extra priority property is used to break ties between nodes in the graph when deciding on
    the order. Nodes with higher priority will try to be placed before nodes with lower priority,
    though this will never cause any strong dependency to be violated.
    """

    @abc.abstractproperty
    def name(self) -> str:
        """The name of this node (should be unique within the graph)"""
        pass

    def __hash__(self) -> int:
        return hash(self.name)

    def __str__(self):
        """Provide a default string representation"""
        return self.name

    @abc.abstractmethod
    def __eq__(self, other):
        """Nodes must define equality"""

    @abc.abstractproperty
    def produces(self) -> Iterable:
        """All data that this node produces"""
        pass

    @abc.abstractproperty
    def requires(self) -> Iterable:
        """All data that this node requires"""
        pass

    @property
    def priority(self) -> int:
        """Nodes with a higher priority will try to be ordered earlier"""
        return 0


class NodeWeakDependencyOrdering:
    """Helper class for doing weak dependency orderings"""

    def __init__(
        self,
        node: BaseNode,
        depends_on_this: Iterable[BaseNode],
        this_depends_on: Iterable[BaseNode],
    ):
        """Create the proxy object

        Parameters
        ----------
        node: BaseNode
            The node whose weak dependencies are being specified
        depends_on_this: Iterable[BaseNode]
            All nodes in this list will compare greater than this
        this_depends_on: Iterable[BaseNode]
            All nodes in thie list will compare less than this

        Any overlap between depends_on_this and this_depends_on will be removed
        """
        self.node = node
        self._depends_on_this = set(depends_on_this)
        self._this_depends_on = set(this_depends_on)
        # Remove any common elements from both
        # We do this because there is nothing to require that the weak
        # dependencies themselves form a DAG so a node could easily appear in
        # both lists.
        common = self._depends_on_this & self._this_depends_on
        self._depends_on_this -= common
        self._this_depends_on -= common

    def __lt__(self, other):
        return other.node in self._depends_on_this

    def __gt__(self, other):
        return other.node in self._this_depends_on

    def __eq__(self, other):
        return not self < other and not self > other

    def __le__(self, other):
        return not self > other

    def __ge__(self, other):
        return not self < other

    def __ne__(self, other):
        return self < other or self > other

    @classmethod
    def from_weak_dep_list(
        cls, node: BaseNode, weak_deps: Iterable[Tuple[BaseNode, BaseNode]]
    ):
        """Create the proxy from a weak dependency list

        Parameters
        ----------
        node: BaseNode
            The node whose weak dependencies are being determined
        weak_deps: Iterable[Tuple[BaseNode, BaseNode]]
            A list of (source, target) pairs describing the weak dependency
        """
        depends_on_this = []
        this_depends_on = []
        for source, destination in weak_deps:
            if source == node:
                depends_on_this.append(destination)
            if destination == node:
                this_depends_on.append(source)
        return cls(node, depends_on_this, this_depends_on)


class NodeAlreadyExistsError(KeyError):
    """Error raised when attempting to define a node that already exists in the graph"""

    def __init__(self, new: BaseNode, existing: BaseNode):
        """Create the exception

        Parameters
        ----------
        new: BaseNode
            The new node being added
        existing: BaseNode
            The existing node
        """
        self.new = new
        self.existing = existing
        super().__init__(
            "A node called '{}' already exists in the graph".format(new.name)
        )


class OutputAlreadyProducedError(ValueError):
    """Error raised when attempting to add a node which produces an output which is already in the graph"""

    def __init__(self, new: BaseNode, existing: BaseNode, data):
        """Create the exception

        Parameters
        ----------
        new : BaseNode
            The new node being added
        existing : BaseNode
            The existing node
        data :
            The duplicated data
        """
        self.new = new
        self.existing = existing
        self.data = data
        super().__init__(
            "Data '{}' produced by new node '{}' is already produced by node '{}'".format(
                data, new.name, existing.name
            )
        )


class CyclicGraphError(ValueError):
    """Error raised when the produced graph would contain a cycle"""

    def __init__(self, cycles: List[List[BaseNode]]):
        """Create the exception

        Parameters
        ----------
        cycles : List[List[BaseNode]]
            The list of cycles
        """
        self.cycles = cycles
        super().__init__(
            "Cycles detected: {}".format(
                [
                    # Make the list actually look like a cycle by repeating the starting node
                    " -> ".join([c.name for c in cycle] + [cycle[0].name])
                    for cycle in cycles
                ]
            )
        )


class UnmetDataDependencyError(Exception):
    """Error raised when the graph builder cannot satisfy a node's data requirement"""

    def __init__(self, node: BaseNode, data: Any):
        """Create the exception

        Parameters
        ----------
        node : BaseNode
            The node whose dependency is not satisfied
        data : Any
            The data that is missing
        """
        self.node = node
        self.data = data
        super().__init__("Dependency {} of node {} not met!".format(data, node))


class GraphBuilder(collections.abc.Mapping):
    """Base class for building the dependency graph

    This class holds a list of the nodes in a graph. The full graph is then created by the
    build_graph method. A full linear ordering obeying any weak dependencies and node priorities
    can be extracted using the order_graph method.

    This class also acts as a mapping from node names to the nodes themselves.

    The main points for base classes to customise this behaviour are:
        - _graph_cls: (class data member) determines which graph class to setup
        - get_producer: Get the node which produces a particular piece of data
        - _populate_graph: Fill the graph with nodes and edges between them
        - _get_weak_deps: Get any weak dependencies (used to order the graph)
        - _get_strong_deps: Get any extra strong dependencies
        - _make_node_key_func: Create functions to order nodes in the graph
    """

    #: The graph class to set up
    _graph_cls = BaseGraph

    def __init__(self):
        """Create the builder"""
        #: Keep track of all the nodes we have
        self._nodes = []
        #: For convenience, keep a mapping from produced information back to the node producing it
        self._by_produced = {}
        #: Strong dependencies are enforced by the graph building and provide a way to force algorithms into a certain order
        self._strong_deps = []
        #: Weak dependencies are used during the graph ordering and provide a way to order algorithms only if that ordering doesn't break the depency ordering
        self._weak_deps = []

    def add_node(self, node: BaseNode):
        """Add a new node into the node list

        If the node already exists this is a no-op

        Raises
        ------
        NodeAlreadyExistsError
            There is a node with the same name that does not compare equal
        OutputAlreadyProducedError
            There is already a node which produces this output
        """
        try:
            existing = self[node.name]
        except KeyError:
            # No node already has that name, keep going
            pass
        else:
            if existing == node:
                # All good, this is just the same node already in the graph
                return
            else:
                raise NodeAlreadyExistsError(node, existing)

        # Now make sure that two nodes don't claim to produce the same data
        for data in node.produces:
            try:
                existing = self.get_producer(data, exact=True)
            except KeyError:
                pass
            else:
                raise OutputAlreadyProducedError(node, existing, data)

        # Now add the new node and record what it produces
        self._nodes.append(node)
        for data in node.produces:
            self._by_produced[data] = node

    def remove_node(self, node: Union[str, BaseNode]):
        """Remove a node from the graph

        Parameters
        ----------
        node : Union[str, BaseNode]
            The node to remove, can be the node object or its name

        Raises
        ------
        KeyError
            The node does not exist in the graph builder
        """
        if isinstance(node, str):
            node = self[node]
        try:
            idx = self._nodes.index(node)
        except ValueError:
            raise KeyError(node)

        del self._nodes[idx]
        for data in node.produces:
            del self._by_produced[data]

    def build_graph(
        self, ignore_unmet=True, extra_producers={}, extra_requirements={}
    ) -> BaseGraph:
        """Create the dependency graph

        Parameters
        ----------
        ignore_unmet: bool
            If False, raise a KeyError if a node depends on data which has no known producer

        Returns
        -------
        The produced dependency graph

        Raises
        ------
        KeyError
            A node depends on data with no known producer and ignore_unmet is False
        CyclicGraphError
            The graph contains cycles
        """
        graph = self._graph_cls._factory()
        self._populate_graph(
            graph,
            ignore_unmet,
            extra_producers=extra_producers,
            extra_requirements=extra_requirements,
        )

        # Make sure that no cycles exist
        self.check_cycles(graph)

        return graph

    def order_graph(self, graph: BaseGraph) -> List[BaseNode]:
        """Calculate the execution order for the graph

        The graph will be ordered using the topological sort algorithm. Ties are broken using the
        function returned by _make_node_key_func

        Parameters
        ----------
        graph: BaseGraph
            The graph to order (should be returned by the build_graph method)

        Returns
        -------
        A list of the nodes in the chosen execution order
        """
        return list(
            networkx.algorithms.dag.lexicographical_topological_sort(
                graph.strong_dependency_graph, self._make_node_key_func()
            )
        )

    def get_producer(self, data, exact: bool = False) -> BaseNode:
        """Get the node which produces a particular data item

        Parameters
        ----------
        data
            The data being queried
        exact : bool
            Some implementations of the builder allow for some data items to alias others. If
            exact is True it bypasses this mechanism, otherwise it does not. By default False

        Raises
        ------
        KeyError: No node produces this data
        """
        return self._by_produced[data]

    def add_weak_dependency(
        self, before: Union[str, BaseNode], after: Union[str, BaseNode]
    ):
        """Add a weak dependency to the graph

        A weak dependency is an ordering hint to the scheduler and will be ignored if it would
        cause a cycle

        Parameters
        ----------
        before: str, BaseNode
            The node which should come first
        after: str, BaseNode
            The node which should come second

        Raises
        ------
        KeyError
            One of the nodes is not in the graph
        """
        if isinstance(before, str):
            before = self[before]
        elif before not in self:
            raise KeyError(before.name)
        if isinstance(after, str):
            after = self[after]
        elif after not in self:
            raise KeyError(after.name)
        self._weak_deps.append((before, after))

    def add_strong_dependency(
        self, before: Union[str, BaseNode], after: Union[str, BaseNode]
    ):
        """Add a strong dependency to the graph

        A strong dependency enforces the order onto the graph and will cause an error if it
        introduces a cycle.

        Parameters
        ----------
        before: str, BaseNode
            The node which will come first
        after: str, BaseNode
            The node which will come second

        Raises
        ------
        KeyError
            One of the nodes is not in the graph
        """
        if isinstance(before, str):
            before = self[before]
        elif before not in self:
            raise KeyError(before.name)
        if isinstance(after, str):
            after = self[after]
        elif after not in self:
            raise KeyError(after.name)
        self._strong_deps.append((before, after))

    @classmethod
    def check_cycles(cls, graph: BaseGraph):
        """Check for cycles in a graph

        Parameters
        ----------
        graph: BaseGraph
            The graph to search

        Raises
        ------
        CyclicGraphError
            Any cycle is found
        """
        cycles = list(networkx.algorithms.cycles.simple_cycles(graph))
        if cycles:
            raise CyclicGraphError(cycles)

    @classmethod
    def annotate_order(
        cls, graph: BaseGraph, order: List[BaseNode], key: str = "TotalOrder"
    ):
        """Annotate edges in the graph with a boolean saying whether or not it is part of the order

        Parameters
        ----------
        graph: BaseGraph
            The graph to annotate
        order: List[BaseNode]
            The ordering of the nodes to annotate. Should be returned by the ``order_graph`` method
        key: str
            The name of the edge attribute under which to store the ordering

        The attribute value will be True for an edge in the order and False otherwise.
        """
        graph.define_edge_attribute(key, False)
        for in_node, out_node in zip(order, order[1:]):
            graph.add_edge(in_node, out_node)
            graph.edges[(in_node, out_node)][key] = True

    def __getitem__(self, name) -> BaseNode:
        try:
            return next(n for n in self._nodes if n.name == name)
        except StopIteration:
            raise KeyError(name)

    def __delitem__(self, name):
        """Remove a node by name"""
        self.remove_node(name)

    def __iter__(self) -> Iterable[str]:
        return (n.name for n in self._nodes)

    def __len__(self) -> int:
        return len(self._nodes)

    @functools.singledispatchmethod
    def __iadd__(self, value) -> "GraphBuilder":
        """Add nodes through the += operator"""
        raise TypeError(type(value))

    @__iadd__.register
    def _(self, value: BaseNode):
        self.add_node(value)
        return self

    @__iadd__.register
    def _(self, value: collections.abc.Mapping):
        for x in value.values():
            self += x
        return self

    @__iadd__.register
    def _(self, value: collections.abc.Iterable):
        for x in value:
            self += x
        return self

    def _add_node_data_dependency(
        self,
        graph: BaseGraph,
        node: BaseNode,
        data: Any,
        extra_producers: Dict[Any, BaseNode] = {},
    ) -> bool:
        """Add an edge to the graph representing a node's data dependency

        Parameters
        ----------
        graph : BaseGraph
            The graph to add the edge to
        node : BaseNode
            The node whose data dependency is being processed
        data : Any
            The data on which the node depends
        extra_producers : Dict[Any, BaseNode], optional
            Extra mapping from data to its producer (used e.g. for communicating information about
            shallow copies)

        Returns
        -------
        bool
            Whether the new edge was added
        """
        try:
            producer = self.get_producer(data, exact=False)
        except KeyError:
            try:
                producer = extra_producers[data]
            except KeyError:
                log.debug("Dependency '%s' of node '%s' is not satisfied", data, node)
                return False
        graph.add_edge(producer, node)
        graph.edges[producer, node]["DataDependencies"].add(data)
        return True

    def _populate_graph(
        self,
        graph: BaseGraph,
        ignore_unmet: bool,
        extra_producers: Dict[Any, BaseNode] = {},
        extra_requirements: Dict[BaseNode, set] = {},
    ):
        """Populate the graph with the nodes contained in this builder

        Parameters
        ----------
        graph : BaseGraph
            The graph to build
        ignore_unmet : bool
            If False, raise an error if any node requires data that another node does not produce.
            Setting this to True implicitly assumes that this data is already available in the
            input file
        extra_producers : Dict[Any, BaseNode], optional
            Extra mapping from data to its producer, by default {}
        extra_requirements : Dict[BaseNode, set], optional
            Extra mapping from a node to extra data required by it, by default {}

        Raises
        ------
        UnmetDataDependencyError
            A node's data dependency is not met (and ignore_unmet is False)
        """
        for node in self.values():
            graph.add_node(node, label=node.name)
            for data in set(node.requires) | set(extra_requirements.get(node, [])):
                if (
                    not self._add_node_data_dependency(
                        graph, node, data, extra_producers
                    )
                    and not ignore_unmet
                ):
                    # _add_node_data_dependency returns False if the dependency is not met
                    raise UnmetDataDependencyError(node, data)

            # Now add extra edges for any additional strong dependencies
            for source, destination in self._get_strong_deps():
                graph.add_edge(source, destination)
                graph.edges[source, destination]["AdditionalStrongDependency"] = True

    def _get_weak_deps(self):
        """Get any additional weak dependencies declared on the graph

        Weak dependencies are used during the graph ordering and provide a way
        to order nodes only if that ordering doesn't break the dependency
        ordering
        """
        return self._weak_deps

    def _get_strong_deps(self):
        """Get any additional strong dependencies declared on the graph

        Strong dependencies are used during the graph building and force nodes
        to be in the order specified
        """
        return self._strong_deps

    def _make_node_key_func(self):
        """The default function to use when ordering nodes

        Returns a function that orders each node according to the following conditions
        - First, if there is a weak dependency between the two nodes, put the source first
        - Second, put the node with a lower priority first
        - Lastly use the same order as they were added into this object
        """
        weak_deps = self._get_weak_deps()

        def node_key(node):
            return (
                NodeWeakDependencyOrdering.from_weak_dep_list(node, weak_deps),
                # Negate the priority so that higher priority comes first
                -node.priority,
                self._nodes.index(node),
            )

        return node_key
