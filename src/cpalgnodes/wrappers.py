"""Helper classes to allow other objects to be interpreted as nodes"""

from typing import Dict, List, Mapping, Set, Union

from cpalgnodes.algnode import AlgNode
from cpalgnodes.componentfacade import (
    Algorithm,
    ConfigFragment,
    PreconfiguredAlgList,
    PreconfiguredComponentAccumulator,
    is_gaudi,
)
from cpalgnodes.containerinfo import ContainerInfo
from cpalgnodes.node import Node


class WrapperNode(Node):
    """Node class for wrapping preconfigured components

    This is a base class for cases where a user provides a (mostly) configured fragment that the
    scheduler can still insert into the schedule based on its dependencies.
    """

    def __init__(
        self,
        *,
        requires_aux: Dict[str, Set[str]] = None,
        produces_aux: Dict[str, Set[str]] = None,
        produces_containers: Dict[str, str] = None,
        has_job_output: bool = False,
        **kwargs,
    ):
        """Create the wrapper

        Parameters
        ----------
        requires_aux : Dict[str, Set[str]], optional
            The auxdata required by this algorithm. The keys for the map are container nicknames
            and the items are the names of the aux items that are required from those containers
        produces_aux : Dict[str, Set[str]], optional
            The auxdata produced by this algorithm. The keys for the map are container nicknames
            and the items are the names of the aux items that are produced on those containers
        produces_containers : Dict[str, str], optional
            The containers produced by this algorithm. The keys are the nicknames of the new
            containers and the keys are the parent containers if the new container is a copy or a
            view or None otherwise.
        has_job_output : bool
            Whether this algorithm provides a job-level output (e.g. TTree/histogram) and must
            always be scheduled, by default False
        """
        super().__init__(**kwargs)
        self._requires_aux = {}  # type: Dict[str, Set[str]]
        if requires_aux is not None:
            self._requires_aux.update(requires_aux)
        self._produces_aux = {}  # type: Dict[str, Set[str]]
        if produces_aux is not None:
            self._produces_aux.update(produces_aux)
        self._produces_containers = {}  # type: Dict[str, str]
        if produces_containers is not None:
            self._produces_containers.update(produces_containers)
        self._has_job_output = has_job_output

    @property
    def requires_aux(self) -> Dict[str, Set[str]]:
        return self._requires_aux

    @property
    def produces_aux(self) -> Dict[str, Set[str]]:
        return self._produces_aux

    @property
    def produces_containers(self) -> Dict[str, str]:
        return self._produces_containers

    @property
    def has_job_output(self) -> bool:
        return self._has_job_output


class PreconfiguredNode(WrapperNode):
    """Node in the graph which contains preconfigured components

    This allows letting the scheduler decide where to run the algorithms based on their
    dependencies while using external code to build the components.
    """

    def __init__(
        self,
        name: str,
        preconfigured,
        priority: int = 0,
        produces_aux: Dict[str, Set[str]] = None,
        produces_containers: Dict[str, str] = None,
        requires_aux: Dict[str, Set[str]] = None,
        has_job_output: bool = False,
    ):
        """Create the node

        Parameters
        ----------
        name : str
            The name for the node
        preconfigured
            Either a ComponentAccumulator or a list of algorithms that is already configured
        priority : int
            The priority of this nodee
        requires_aux : Dict[str, Set[str]], optional
            The auxdata required by this algorithm. The keys for the map are container nicknames
            and the items are the names of the aux items that are required from those containers
        produces_aux : Dict[str, Set[str]], optional
            The auxdata produced by this algorithm. The keys for the map are container nicknames
            and the items are the names of the aux items that are produced on those containers
        produces_containers : Dict[str, str], optional
            The containers produced by this algorithm. The keys are the nicknames of the new
            containers and the keys are the parent containers if the new container is a copy or a
            view or None otherwise.
        has_job_output : bool
            Whether this algorithm provides a job-level output (e.g. TTree/histogram) and must
            always be scheduled, by default False
        """
        super().__init__(
            requires_aux=requires_aux,
            produces_aux=produces_aux,
            produces_containers=produces_containers,
            has_job_output=has_job_output,
        )
        self._name = name
        self._preconfigured = preconfigured
        self._priority = priority

    @property
    def name(self) -> str:
        return self._name

    def __eq__(self, other):
        return self.name == other.name and self._preconfigured == other._preconfigured

    def __hash__(self) -> int:
        return hash(self.name)

    @property
    def priority(self) -> int:
        return self._priority

    def create(
        self, container_info: Mapping[str, ContainerInfo]
    ) -> Union[Algorithm, List[Algorithm], ConfigFragment]:
        if is_gaudi:
            from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

            if isinstance(self._preconfigured, ComponentAccumulator):
                return PreconfiguredComponentAccumulator(self._preconfigured)
        return PreconfiguredAlgList(self._preconfigured)


class UserAlgNode(WrapperNode, AlgNode):
    """Node that wraps a single user-configured algorithm"""

    def __init__(
        self,
        type: str,
        name: str,
        priority: int = 0,
        inputs: Dict[str, str] = None,
        outputs: Dict[str, str] = None,
        produces_aux: Dict[str, Set[str]] = None,
        produces_containers: Dict[str, str] = None,
        requires_aux: Dict[str, Set[str]] = None,
        has_job_output: bool = False,
        **properties,
    ):
        """Create the algorithm

        Parameters
        ----------
        type: str
            The concrete algorithm type to create
        name: str
            The name of the algorithm to create
        priority: int
            The scheduler will try to put nodes with a higher priority first
        inputs: Dict[str, str]
            Mapping from a property on the algorithm to the nickname of the input that will be
            provided to it
        outputs: Dict[str, str]
            Mapping from a property on the algorithm to the nickname of the output that will be
            provided to it
        requires_aux : Dict[str, Set[str]], optional
            The auxdata required by this algorithm. The keys for the map are container nicknames
            and the items are the names of the aux items that are required from those containers
        produces_aux : Dict[str, Set[str]], optional
            The auxdata produced by this algorithm. The keys for the map are container nicknames
            and the items are the names of the aux items that are produced on those containers
        produces_containers : Dict[str, str], optional
            The containers produced by this algorithm. The keys are the nicknames of the new
            containers and the keys are the parent containers if the new container is a copy or a
            view or None otherwise.
        has_job_output : bool
            Whether this algorithm provides a job-level output (e.g. TTree/histogram) and must
            always be scheduled, by default False
        **properties : Dict[str, Any]
            Any further properties to be set on the algorithm
        """
        super().__init__(
            type=type,
            name=name,
            priority=priority,
            inputs=inputs,
            outputs=outputs,
            requires_aux=requires_aux,
            produces_aux=produces_aux,
            produces_containers=produces_containers,
            has_job_output=has_job_output,
            **properties,
        )
