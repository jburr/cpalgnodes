"""Basic algorithm to output ntuples"""

# TODO - does this need to split up into separate algorithms somehow?

from collections import defaultdict
from typing import Dict, List, Mapping, Set, Tuple, Union

import boolean

from cpalgnodes.booleanalgebra import algebra
from cpalgnodes.componentfacade import Algorithm, ConfigFragment
from cpalgnodes.graph import BaseNode
from cpalgnodes.node import ContainerInfo, Node
from cpalgnodes.utils import add_sys_pattern


class NTupleMakerNode(Node):
    def __init__(self, tree_name: str):
        """Create the tree maker node"""
        self._tree = tree_name
        self._container_selections: Dict[str, boolean.Expression] = defaultdict(
            lambda: Node.ALL_OBJECTS
        )
        self._branches: Dict[str, List[Tuple[str, str]]] = defaultdict(list)

    def set_container_selection(
        self, container: str, selection: Union[str, boolean.Expression]
    ):
        """Set the selection on a container

        Only objects passing this selection (on any systematic) will be written out
        """
        if not isinstance(selection, boolean.Expression):
            selection = algebra.parse(selection)
        self._container_selections[container] = selection

    def add_branch(self, container: str, aux_name: str, branch_name: str):
        """Add a single branch to the output

        Parameters
        ----------
        container : str
            The nickname of the container
        aux_name: str
            The name of the aux data to write
        branch_name: str
            The name of the output branch
        """
        self._branches[container].append((aux_name, branch_name))

    def add_branches(self, container: str, branches: Tuple[str, str]):
        """Add several branches to the output

        Parameters
        ----------
        container : str
            The nickname of the input container
        branches : Tuple[str, str]
            List of (aux_name, branch_name) tuples
        """
        self._branches[container] += branches

    @property
    def name(self):
        return f"{self._tree}TreeNode"

    def __eq__(self, other):
        return (
            type(self) == type(other)
            and self._tree == other._tree
            and self._container_selections == other._container_selections
            and self._branches == other._branches
        )

    __hash__ = BaseNode.__hash__

    @property
    def produces_containers(self) -> Dict[str, str]:
        return {}

    @property
    def produces_aux(self) -> Dict[str, Set[str]]:
        return {}

    @property
    def requires_aux(self) -> Dict[str, Set[str]]:
        requires = defaultdict(set)
        for cont, sel in self._container_selections.items():
            requires[cont] |= set(str(symbol) for symbol in sel.symbols)
        for cont, branches in self._branches.items():
            for branch_in, _ in branches:
                requires[cont].add(branch_in)
        return requires

    def requires_objects(
        self, required_output: Dict[Tuple[str, str], boolean.Expression]
    ) -> Dict[Tuple[str, str], boolean.Expression]:
        return {
            (cont, aux): self._container_selections[cont] for cont, aux in self.requires
        }

    def create(
        self, container_info: Mapping[str, ContainerInfo]
    ) -> Union[Algorithm, List[Algorithm], ConfigFragment]:
        algs = []
        algs.append(
            Algorithm(
                "CP::TreeMakerAlg", f"{self._tree}TreeMakerAlg", TreeName=self._tree
            )
        )
        rename = {}
        for container, sel in self._container_selections.items():
            if sel == Node.ALL_OBJECTS:
                continue
            info = container_info[container]
            output_name = f"{self._tree}{container}Preselection_%SYS%"
            selection = f"{self._tree}{container}UnionPreselection,as_char"
            rename[container] = output_name
            algs += [
                Algorithm(
                    "CP::AsgUnionSelectionAlg",
                    f"{self._tree}{container}UnionPreselectionAlg",
                    particles=info.input_name,
                    preselection=info.selection_to_property_value(
                        self._container_selections[container]
                    ),
                    selectionDecoration=selection,
                ),
                Algorithm(
                    "CP::AsgViewFromSelectionAlg",
                    f"{self._tree}{container}PreselectionAlg",
                    input=info.input_name,
                    output=output_name,
                    selection=[selection],
                ),
            ]
        branches = []
        for container, branch_list in self._branches.items():
            output_name = rename.get(container, container_info[container].input_name)
            for branch_in, branch_out in branch_list:
                branch_out = add_sys_pattern(branch_out)
                branches.append(f"{output_name}.{branch_in} -> {branch_out}")

        algs.append(
            Algorithm(
                "CP::AsgxAODNTupleMakerAlg",
                f"{self._tree}NTupleMakerAlg",
                TreeName=self._tree,
                Branches=branches,
            )
        )

        algs.append(
            Algorithm(
                "CP::TreeFillerAlg", f"{self._tree}TreeFillerAlg", TreeName=self._tree
            )
        )
        return algs

    @property
    def has_job_output(self) -> bool:
        return True
