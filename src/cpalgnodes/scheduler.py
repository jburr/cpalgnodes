"""Class to control the scheduling of the job and additional configuration based on the order"""

import copy
import itertools
import logging
from collections import defaultdict
from typing import Dict, Iterable, List, Mapping, Optional, Set, Tuple, Union

import boolean
import networkx

from cpalgnodes.booleanalgebra import PRESELECTED_OBJECTS, algebra
from cpalgnodes.componentfacade import JobConfiguration
from cpalgnodes.containerinfo import ContainerInfo, mk_temp_name
from cpalgnodes.graph import BaseGraph, GraphBuilder, OutputAlreadyProducedError
from cpalgnodes.node import Dependency, Node
from cpalgnodes.selection import Selection

log = logging.getLogger(__name__)


def add_edge_to_dag(graph: networkx.DiGraph, source, destination):
    """Add an edge between two nodes, raising an error if doing so would create a cycle"""
    if source in graph and destination in networkx.ancestors(graph, source):
        raise ValueError(f"Adding edge {source} -> {destination} would create a cycle!")
    graph.add_edge(source, destination)


class Scheduler(GraphBuilder):
    """Class to build the dependency graph and create all the algorithms

    After the graph has been configured and the order determined, the preselections necessary for
    each node are determined.
    """

    def __init__(self):
        super().__init__()
        # Record which nodes create which containers
        self._container_producers = {}
        # Record the relationships between containers in the job, i.e. where one containerr is
        # copied from anotherr
        self._container_hierarchy = networkx.DiGraph()
        # Keep track of the selections that have been defined on this. Mapping from container to
        # a mappiing from the selection name to the selection object
        self._object_selections: Dict[str, Dict[str, Selection]] = defaultdict(dict)
        # Dummy input and output nodes
        self._input_node = InputNode()
        self._output_node = OutputNode()
        self += [self._input_node, self._output_node]

    def add_input(self, container: str, auxitems: Iterable[str] = ()):
        """Tell the scheduler about a container available on the input

        Parameters
        ----------
        container: str
            The name of the container
        auxitems: Iterable[str]
            Any auxdata available on the container
        """
        # Make sure nothing else already produces this
        for aux in itertools.chain(auxitems, (None,)):
            try:
                existing = self.get_producer((container, aux))
            except KeyError:
                pass
            else:
                raise OutputAlreadyProducedError(
                    self._input_node, existing, (container, aux)
                )
        cont_aux = self._input_node.input_aux.setdefault(container, set())
        cont_aux |= set(auxitems)
        # Also register the input as the producer for all of these
        self._by_produced[(container, None)] = self._input_node
        for aux in auxitems:
            self._by_produced[(container, aux)] = self._input_node

    def set_output_selection(
        self, container: str, selection: Union[str, boolean.Expression]
    ):
        """Set the selection on the output container"""
        if not isinstance(selection, boolean.Expression):
            selection = algebra.parse(selection)
        self._output_node.container_selections[container] = selection

    def add_output(
        self,
        container: str,
        auxitems: Union[Iterable[str], Mapping[str, boolean.Expression]] = (),
    ):
        """Tell the scheduler about a container we need available on the output

        Parameters
        ----------
        container: str
            The name of the container
        auxitems: Union[Iterable[str], Mapping[str, boolean.expression]]
            Any aux items needed on the output. Can either be a a list of auxitems or a mapping
            from aux item to the required output selection for that item.
        """
        cont_aux = self._output_node.outputs.setdefault(container, set())
        cont_aux |= set(auxitems)
        if isinstance(auxitems, Mapping):
            for aux, presel in auxitems.items():
                if not isinstance(presel, boolean.Expression):
                    presel = algebra.parse(presel)
                self._output_node.output_selections[container, aux] = presel

    def add_node(self, node: Node):
        super().add_node(node)
        for container, parent in node.produces_containers.items():
            # Skip any that are just updating a container
            if container in node.requires_containers:
                continue
            if container in self._container_producers:
                raise ValueError(
                    f"Container '{container}' produced by '{node}' is already produced by '{self._container_producers[container]}'!"
                )
            if parent is not None:
                add_edge_to_dag(self._container_hierarchy, parent, container)
            else:
                self._container_hierarchy.add_node(container)
            self._container_producers[container] = node
        node._on_add(self)

    def remove_node(self, node: Node):
        super().remove_node(node)
        # If this node creates new containers remove them from the hierarchy
        # where appropriate
        for container, parent in node.produces_containers:
            del self._container_producers[container]
            if parent is not None:
                self._container_hierarchy.remove_edge(parent, container)
                # If this leaves either node without any edges remove it
                if networkx.degree(self._container_hierarchy, parent) == 0:
                    self._container_hierarchy.remove_node(parent)
                if networkx.degree(self._container_hierarchy, container) == 0:
                    self._container_hierarchy.remove_node(container)
        node._on_remove(self)

    def get_container_parent(self, container: str) -> Optional[str]:
        """Get the parent of a container

        Return None if it has no known parent
        """
        in_edges = self._container_hierarchy.in_edges(container)
        assert (
            len(in_edges) < 2
        ), f"{len(in_edges)} input edges for container '{container}' - should be impossible!"

        val = next(iter(in_edges), (None, None))
        return val[0]

    def get_producer(self, data, exact: bool = False) -> Node:
        # Allow searching back through the container hierarchy
        container, aux = data
        while container is not None:
            try:
                return super().get_producer((container, aux))
            except KeyError:
                if not exact:
                    container = self.get_container_parent(container)
                else:
                    raise
        else:
            raise KeyError(data)

    def get_selection(self, container: str, name: str) -> Selection:
        """Get the selection object

        Parameters
        ----------
        container : str
            The (nick)name of the container
        name : str
            The name of the selection

        Raises
        ------
        KeyError
            No such selection exists

        Returns
        -------
        Selection
            The corresponding selection object
        """
        return self._object_selections[container][name]

    def flatten_selection(
        self, container: str, selection: str, _seen: List[str] = None
    ) -> List[str]:
        """Expand a selection into a list of selections implicit in this one

        Parameters
        ----------
        container : str
            The nickname of the container that the selection is defined on
        selection : str
            The name of the selection
        _seen : Set[str], optional
            Internal parameter to detector cyclic definitions

        Raises
        ------
        ValueError
            A cyclic definition is detected
        KeyError
            One of the selections involved is unknown
        """
        if _seen is None:
            _seen = []
        elif selection in _seen:
            raise ValueError(f"Cycle detected! {' -> '.join(_seen + [selection])}")

        # Gather the full list of selections
        full_selections = []
        # Allow searching through parent containers for the names
        while container is not None:
            try:
                sel = self.get_selection(container, selection)
                break
            except KeyError:
                container = self.get_container_parent(container)
        else:
            raise KeyError(selection)

        for previous in sel.previous:
            # Flatten all of these as well
            for subsel in self.flatten_selection(
                container, previous, _seen + [selection]
            ):
                # Only add the selection if it isn't already present
                if subsel not in full_selections:
                    full_selections.append(subsel)
        # Also add the selection name itself to the end
        full_selections.append(selection)
        return full_selections

    def add_selection(self, container: str, selection: Selection):
        """Tell the scheduler about a new selection

        Parameters
        ----------
        container : str
            The (nick)name of the container that this operates on
        selection : Selection
            The selection

        Raises
        ------
        KeyError
            The container.selection_name pair is already defined
        """
        try:
            self.get_producer((container, selection.name), exact=True)
        except KeyError:
            log.warn(
                f"Cannot find the producer for the selection {container}.{selection.name}"
            )
        if selection.name in self._object_selections[container]:
            raise KeyError(f"Selection {container}.{selection.name} already exists!")
        self._object_selections[container][selection.name] = selection

    def rm_selection(self, container: str, selection_name: str):
        """Remove a selection from the scheduler

        Parameters
        ----------
        container : str
            The container the selection is defined on
        selection_name : str
            The name of the selection

        Raises
        ------
        KeyError
            The selection does not exist
        """
        del self._object_selections[container][selection_name]

    def build_graph(
        self, ignore_unmet=True, extra_producers={}, extra_requirements={}
    ) -> BaseGraph:
        graph = super().build_graph(ignore_unmet, extra_producers, extra_requirements)
        # Add edges to the output for any node which provides a job output
        for node in self.values():
            if node.has_job_output:
                graph.add_edge(node, self._output_node)
        return graph

    def create_schedule(
        self,
        inputs: Mapping[str, str],
        outputs: Mapping[str, str],
        ignore_unmet: bool = True,
        prune_unused: bool = True,
        warn_unused: bool = True,
        annotate_order: str = None,
    ) -> Tuple[JobConfiguration, BaseGraph, Tuple[Node]]:
        """Create the scheduled list of algorithms

        Parameters
        ----------
        inputs : Mapping[str, str]
            Mapping from container nickname to the real input name in the StoreGate
        outputs : Mapping[str, str]
            Mapping from container nickname to the real output name in the StoreGate
        ignore_unmet : bool, optional
            If False, raise an exception where a dependency is not met. True implicitly assumes
            that unmet dependencies are available on the input, by default True
        prune_unused : bool, optional
            If True, remove any nodes from the graph whose outputs are not used, by default True
        warn_unused : bool, optional
            If True, warn if there are any nodes whose outputs are not used, by default True
        annotate_order : str, optional
            If set, add an edge attribute to the graph saying if an edge is part of the order

        Returns
        -------
        JobConfiguration
            The full job configuration
        BaseGraph
            The built dependency graph
        Tuple[Node]
            Any nodes whose outputs are not used (even if these have been pruned from the graph)
        """
        # Keep track of data dependencies which are not taken care of by the graph nodes
        extra_producers = {}
        extra_requirements = {}
        # Anything we've been told is an input must be provided by the input node
        extra_producers.update(
            {(container, None): self._input_node for container in inputs}
        )
        # Anything we've been told is an output should be output
        extra_requirements.update(
            {(container, None): self._output_node for container in outputs}
        )

        # Build the basic dependency graph
        graph = self.build_graph(
            ignore_unmet=ignore_unmet,
            extra_producers=extra_producers,
            extra_requirements=extra_requirements,
        )
        unused = self._process_unused(
            graph, prune_unused=prune_unused, warn_unused=warn_unused
        )
        # Calculate the correct iteration order
        node_order: List[Node] = self.order_graph(graph)
        if annotate_order is not None:
            self.annotate_order(graph, node_order, key=annotate_order)

        last_updaters, node_preselections = self._preprocess_nodes(node_order)
        # Now we can go through and create the whole schedule
        schedule = JobConfiguration()
        container_infos = defaultdict(ContainerInfo)
        # Fill in basic information into the container infos
        # Start off each container with its input name
        for nickname, sg_name in inputs.items():
            container_infos[nickname].input_name = sg_name
        # Add the selection counts
        for nickname, selections in self._object_selections.items():
            info = container_infos[nickname]
            for selection in selections.values():
                info.selection_counts[selection.name] = selection.count

        for node in node_order:
            for container, preselection in node_preselections[node].items():
                container_infos[container].preselection = preselection
            for nickname in node.produces_containers:
                output = outputs.get(nickname)
                container_infos[nickname].output_name = (
                    output if node is last_updaters[nickname] else mk_temp_name(output)
                )
            schedule += node.create(container_infos)
            # Now update the inputs to the new output
            for nickname in node.produces_containers:
                info = container_infos[nickname]
                info.input_name = info.output_name

        return schedule, graph, unused

    def _add_node_data_dependency(
        self,
        graph: BaseGraph,
        node: Node,
        data: Dependency,
        extra_producers: Dict[Dependency, Node] = {},
    ) -> bool:
        # Need to handle cases where the auxdata is actually on a parent container
        container, aux = data
        stack = []
        while container is not None:
            stack.append(container)
            try:
                producer = self.get_producer(data, exact=True)
                break
            except KeyError:
                container = self.get_container_parent(container)
        else:
            try:
                producer = extra_producers[data]
            except KeyError:
                log.debug("Dependency '%s' of node '%s' is not satisfied", data, node)
                return False
        source = producer
        for container in reversed(stack[1:]):
            target = self.get_producer((container, None), exact=True)
            graph.add_edge(source, target)
            graph.edges[source, target]["DataDependencies"].add((container, aux))
            source = target
        graph.add_edge(source, node)
        graph.edges[source, node]["DataDependencies"].add(data)
        return True

    def _process_unused(
        self, graph: BaseGraph, prune_unused: bool = True, warn_unused: bool = True
    ) -> Tuple[Node]:
        """Process the unused algorithms

        Parameters
        ----------
        prune_unused : bool, optional
            If True, remove any nodes from the graph whose outputs are not used, by default True
        warn_unused : bool, optional
            If True, warn if there are any nodes whose outputs are not used, by default True

        Returns
        -------
        The unused nodes
        """
        # Find the unused nodes in the graph. A node is used iff it's an ancestor of the output node
        used_nodes = networkx.algorithms.dag.ancestors(graph, self._output_node) | {
            self._output_node
        }
        unused = tuple(set(graph) - set(used_nodes))
        if unused:
            if warn_unused:
                log.warn("Unused nodes found:")
                for node in unused:
                    log.warn(node)
            if prune_unused:
                graph.remove_nodes_from(unused)
            else:
                # If we're not going to prune the unused edges, then add edges from them to the output
                # node. We will rely on all paths ending at the output node.
                for node in unused:
                    if graph.out_degree(node) == 0:
                        # We only do this for the leaf nodes, i.e. those without a following node.
                        # Other nodes must necessarily eventually lead to a current leaf node (we have
                        # no cycles in the graph)
                        graph.add_edge(node, self._output_node)

    def _preprocess_nodes(
        self, nodes: List[Node]
    ) -> Tuple[Dict[str, Node], Dict[Node, Dict[str, boolean.Expression]]]:
        """Extract extra information from the ordered list of nodes

        Parameters
        ----------
        nodes : List[Node]
            The ordered list of nodes

        Returns
        -------
        Dict[str, Node]
            Which node is the last to update any particular container
        Dict[Node, Dict[str, boolean.Expression]]
            Preselections per container, per node
        """
        # Output variables
        last_updaters: Dict[str, Node] = {}
        node_preselections: Dict[Node, Dict[str, boolean.Expression]] = defaultdict(
            lambda: defaultdict(lambda: PRESELECTED_OBJECTS)
        )

        # Internal variables
        # For each piece of data, on what selection of objects is it required
        required_objects: Dict[Dependency, boolean.Expression] = defaultdict(
            lambda: PRESELECTED_OBJECTS
        )
        # Any selections which have been produced downstream of the current position and are
        # therefore unavailable. Split by container
        unavailable: Dict[str, Set[boolean.Expression]] = defaultdict(set)

        # First create substitutions for all existing definitions/selections
        selection_substitutions: Dict[
            str, Dict[boolean.Symbol, algebra.AND]
        ] = defaultdict(dict)

        # Work out which nodes are ultimately responsible for which selections
        selection_producers: Dict[Node, List[Dependency]] = defaultdict(list)
        for container, selections in self._object_selections.items():
            for sel_name in selections:
                selection_producers[
                    self.get_producer((container, sel_name), exact=False)
                ] = (
                    container,
                    sel_name,
                )
                flattened = self.flatten_selection(container, sel_name)
                # Only add selections that have a non-trivial expansion
                if len(flattened) > 1:
                    selection_substitutions[container][
                        algebra.Symbol(sel_name)
                    ] = algebra.AND(*map(algebra.Symbol, flattened))

        # Now go back through the nodes in reverse order
        for node in reversed(nodes):
            # Mark which node is the last to touch a container
            for container in node.produces_containers:
                last_updaters.setdefault(container, node)

            # Figure out which objects upstream nodes need to provide
            for (container, aux), preselection in node.requires_objects(
                {data: required_objects[data] for data in node.produces}
            ).items():
                required_objects[container, aux] |= preselection.subs(
                    selection_substitutions[container]
                )

            # Figure out which selections are now unavailable to upstream nodes
            for container, selection in selection_producers.get(node, ()):
                unavailable[container].add(algebra.Symbol(selection))

            # Figure out the preselections for this node based on its inputs
            for container, auxlist in node.produces_aux.items():
                preselection = PRESELECTED_OBJECTS
                for aux in auxlist:
                    presel |= required_objects[container, aux]
                node_preselections[node][container] = algebra.remove_symbols(
                    preselection, unavailable[container]
                )

        # Finally, go back through the calculated preselections and substitute the definitions
        # where possible.
        # The substitution works as follows:
        # 1. Convert the existing preselection into the disjunctive normal form (this is
        #    basically an OR of ANDs)
        # 2. For each of those ANDs, check if each definition is a subset of the symbols in it.
        #    If it is, replace those symbols with the definition
        # In order to make sure the longest possible working point is used wherever possible we
        # will sort the working points based on set comparison operators
        sorted_definitions = {
            container: sorted(
                [(set(subsels.args), name) for name, subsels in selections.items()],
                # Reverse ensures longest first
                reverse=True,
            )
            for container, selections in selection_substitutions.items()
        }
        for presel_dict in node_preselections.values():
            for container, preselection in presel_dict.items():
                presel_dict[container] = _replace_definitions(
                    preselection, sorted_definitions.get(container, [])
                )

        return last_updaters, node_preselections


class InputNode(Node):
    """Node representing the input to the scheduler

    This is a dummy node (adds no algorithms) in the graph that tells the scheduler what input
    information is available
    """

    def __init__(self):
        self.input_aux = {}

    @property
    def name(self):
        return "__INPUTNODE__"

    @property
    def produces_aux(self):
        return self.input_aux

    @property
    def produces_containers(self):
        return {k: None for k in self.input_aux}

    @property
    def requires_aux(self):
        return {}

    def requires_objects(
        self, required_output: Dict[Tuple[str, str], boolean.Expression]
    ) -> Dict[Tuple[str, str], boolean.Expression]:
        return {}

    def create(self, container_info):
        return []

    def __eq__(self, other):
        return self is other

    __hash__ = Node.__hash__


class OutputNode(Node):
    """Node representing the output of the scheduler

    This is a dummy node (adds no algorithms) in the graph that represents the output. It produces
    no data and has all the required output information as required data dependencies
    """

    def __init__(self):
        self.outputs = {}
        self.container_selections = defaultdict(lambda: Node.ALL_OBJECTS)
        self.output_selections = defaultdict(lambda: Node.ALL_OBJECTS)

    @property
    def name(self) -> str:
        return "__OUTPUTNODE__"

    @property
    def produces_aux(self):
        return {}

    @property
    def produces_containers(self):
        return {}

    @property
    def requires_aux(self):
        required = copy.copy(self.outputs)
        for container, selection in self.container_selections.items():
            required.setdefault(container, {})
            for symbol in selection.symbols:
                required[container].add(str(symbol))
        return required

    def requires_objects(
        self, required_output: Dict[Tuple[str, str], boolean.Expression]
    ) -> Dict[Tuple[str, str], boolean.Expression]:
        return {
            (cont, aux): (
                (self.output_selections[cont, aux] & self.container_selections[cont])
                | required_output.get((cont, aux), Node.PRESELECTED_OBJECTS)
            ).simplify()
            for cont, aux in self.requires
        }

    def create(self, container_info):
        return []

    def __eq__(self, other):
        return self is other

    __hash__ = Node.__hash__


def _replace_definitions(
    expr: boolean.Expression,
    definitions: List[Tuple[Set[boolean.Symbol], boolean.Symbol]],
) -> boolean.Expression:
    """Replace parts of expressions with matching definitions

    For instance if we had a definition 'baseline = looseID & looseIso & baselineKinematic' the
    selection 'looseID & looseIso & passOR & baselineKinematic' would become 'baselilne & passOR'

    Parameters
    ----------
    expr : boolean.Expression
        The expression to substitute
    definitions : List[Tuple[set(boolean.Symbol), boolean.Symbol]]
        The definitions to substitute in. Should be a list of the expressions and the corresponding
        definition name, sorted by the order in which the substitutions should be attempted

    Returns
    -------
    boolean.Expression
        The simplified expression
    """
    expr = algebra.dnf(expr)
    if isinstance(expr, algebra.AND):
        symbols = set(expr.args)
        for selections, name in definitions:
            if selections <= symbols:
                # This means that the expression completely contains the definition => make the
                # substitution
                symbols = (symbols - selections) | {name}
        if len(symbols) == 0:
            return algebra.TRUE
        elif len(symbols) == 1:
            return next(iter(symbols))
        else:
            return algebra.AND(*symbols).simplify()
    elif isinstance(expr, algebra.OR):
        return algebra.OR(
            _replace_definitions(expr2, definitions) for expr2 in expr.args
        ).simplify()
    else:
        return expr
