"""Classes to collect together the configurations of algorithms, tools and services

Jobs in Athena and AnalysisBase are built out of components. Components are written in C++ but all
expose values which can be configured called 'properties'. The classes in this module model these
through dictionary-like access. Components come in three main types:

Algorithms:
    Algorithms are the primary pieces of the job. They all have an execute method which is called
    once per event (unless the event has been rejected by an upstream algorithm).

Tools:
    Tools are more flexible components. Their methods can conform to any interface and must
    (ultimately) be called by algorithms to do their jobs. Tools can be either public or private.
    Public tools are globally available and accessible by name. This means that there is only one
    instance of any named public tool. Generally public tools are no longer recommended with a few
    exceptions (such as the TrigDecisionTool which must *always* be public). Private tools are
    owned by their parent components.
Services:
    Services are also global components. TODO: Properly describe the difference between public
    tools and services...
"""

import abc
import enum
from collections.abc import MutableMapping, MutableSequence
from typing import Dict, Iterable, List, Optional, Set, Union

from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, createPublicTool, createService
from AnaAlgorithm.Logging import logging

try:
    import Gaudi

    is_gaudi = True
except ImportError:
    is_gaudi = False
else:
    from AthenaConfiguration.ComponentFactory import CompFactory
    from AthenaConfiguration.LegacySupport import appendCAtoAthena

log = logging.getLogger(__name__)


class Component(MutableMapping):
    """Base class for components

    Dictionary-like access is used to set/retrieve properties.

    Extra components (e.g. services) that this requires can be provided to the None key.
    """

    class ComponentType(enum.Enum):
        Algorithm = enum.auto()
        PublicTool = enum.auto()
        PrivateTool = enum.auto()
        Service = enum.auto()

        @property
        def is_public(self) -> bool:
            return self in [PublicTool, Service]

    def __init__(
        self, type: str, name: str, component_type: ComponentType, **properties
    ):
        """Create the component

        Parameters
        ----------
        type: str
            The type of the component
        name: str
            The name of the component
        component_type: Component.ComponentType
            The type of component (algorithm, public tool, private tool or service)
        **properties
            Any properties to be set immediately
        """
        # When the 'real' component is created it will be stored here
        self.__component = None
        self._type = type
        self._name = name
        self._component_type = component_type
        self._properties = {}
        self.update(properties)
        # Extra components that this needs but are not directly set as properties
        self.__extra_components = []  # type: List[Component]

    def get(
        self, sequence: Union[AlgSequence, str] = None, accumulator=None, flags=None
    ):
        """Get the real underlying component, creating it if it hasn't been already

        The first time this is called will lock the component, preventing further modification

        Parameters
        ----------
        sequence : Union[AlgSequence, str]
            If running in AnalysisBase, the AlgSequence object must be provided as a hook for the
            public components. If running in athena in the ComponentAccumulator mode a sequence
            name can be provided and the algorithms will be placed in that sequence. In this case
            it must already exist in the accumulator.
        accumulator : ComponentAccumulator, optional
            If running in athena, a ComponentAccumulator-based configuration can be used instead.
            To activate this mode pass the accumulator the fragment should be merged into here.
            When running in this mode the ConfigFlags should also be provided
        flags : AthConfigFlags, optional
            The ConfigFlags instance when running in the ComponentAccumulator mode
        """
        if accumulator is not None:
            if flags is None:
                raise ValueError(
                    "The flags must be provided if running in ComponentAccumulator mode"
                )
            if not is_gaudi:
                raise ValueError("ComponentAccumulators cannot be used in AnalysisBase")
            if sequence is None:
                sequence = "AthAlgSeq"
        if self.__component is None:
            log.debug(f"Create {self.type_and_full_name}")
            self.__component = self._create_component(sequence, accumulator, flags)
            self._set_props(self.__component, sequence, accumulator, flags)
            for component in self.__extra_components:
                component.get(sequence, accumulator, flags)
        return self.__component

    @property
    def type(self) -> str:
        """The type of the component"""
        return self._type

    @property
    def name(self) -> str:
        """The name of the component"""
        return self._name

    @property
    def type_and_name(self) -> str:
        """The type/name string"""
        return f"{self.type}/{self.name}"

    @property
    def full_name(self) -> str:
        """The full name of the component (including its parent)"""
        return self.name

    @property
    def type_and_full_name(self) -> str:
        """The type/full_name string"""
        return f"{self.type}/{self.full_name}"

    @property
    def component_type(self) -> ComponentType:
        """The component type (algorithm, public/private tool or service)"""
        return self._component_type

    @property
    def is_public(self) -> bool:
        """Is this component public"""
        return self.component_type.is_public

    @property
    def locked(self) -> bool:
        """Whether the component can still be modified"""
        return self.__component is not None

    def __getitem__(self, key: str):
        """Get a set property"""
        if key is None:
            return self.__extra_components
        head, sep, tail = key.partition(".")
        if sep:
            return self._properties[head][tail]
        else:
            return self._properties[head]

    def __setitem__(self, key: str, value):
        """Set a property value"""
        if self.locked:
            raise ValueError(f"Cannot modify locked component {self.full_name}")
        if key is None:
            self.__extra_components.append(value)
            return
        head, sep, tail = key.partition(".")
        if sep:
            self._properties[head][tail] = value
        else:
            if isinstance(value, (PrivateTool, PrivateToolArray)):
                value.set_parent(self, head)
            self._properties[head] = value

    def __delitem__(self, key: str):
        """Remove a set property value"""
        if self.locked:
            raise ValueError(f"Cannot modify locked component {self.full_name}")
        head, sep, tail = key.partition(".")
        if sep:
            del self._properties[head][tail]
        else:
            del self._properties[head]

    def __iter__(self):
        return iter(self._properties)

    def __len__(self):
        return len(self._properties)

    def __eq__(self, other: "Component"):
        return (
            self.name == other.name
            and self.type == other.type
            and self.component_type == other.component_type
            and self._properties == other._properties
        )

    def __hash__(self):
        # Define hash functions so these can be stored in dicts/sets. All components full names
        # should be unique
        return hash(self.full_name)

    def __str__(self):
        return self.type_and_name

    def merge(self, other: "Component"):
        """Merge this with another component definition with the same name

        The default implementation will make sure that the type, name and component type are the
        same and then merges the properties. If a property is present in both it must be equal.

        Note that this implementation is ignored when ComponentAccumulators are used

        Raises
        ------
        ValueError
            The two components are not equal
        """
        if (
            self.name != other.name
            or self.type != other.type
            or self.component_type != other.component_type
        ):
            raise ValueError("Cannot merge unequal components!")
        for k, v in other.items:
            try:
                existing = self[k]
            except KeyError:
                self[k] = v
            else:
                if isinstance(existing, Component):
                    existing.merge(v)
                elif existing != v:
                    raise ValueError(
                        f"Property {k} of {self.full_name} does not match!"
                    )

    def subcomponents(self) -> Iterable["Component"]:
        """Iterate over all subcomponents

        A subcomponent is any component that is a property (or a property of a property) of this
        component
        """
        for x in self.values():
            if isinstance(x, Component):
                yield x
                yield from x.subcomponents()
            elif isinstance(x, Iterable):
                for y in x:
                    if isinstance(y, Component):
                        yield y
                        yield from y.subcomponents()
        yield from self.__extra_components

    @abc.abstractmethod
    def copy(self) -> "Component":
        """Create a copy of this component"""

    def setPrivateTool(self, key: str, type: str, **properties) -> "PrivateTool":
        """Set the named property to a private tool

        Parameters
        ----------
        key : str
            The property on the parent Component to set
        type : str
            The type of the private tool
        **properties
            Any other properties to be set on the private tool

        Returns
        -------
        The created private tool
        """
        self[key] = PrivateTool(type, key, **properties)
        return self[key]

    def appendToPrivateToolArray(
        self, key: str, type: str, name: str, **properties
    ) -> "PrivateTool":
        """Append a new private tool to the named property

        Parameters
        ----------
        key : str
            The property on the parent Component to set
        type : str
            The type of the private tool
        name : str
            The name of the new private tool (ignored in AnalysisBase)
        **properties
            Any other properties to be set on the private tool

        Returns
        -------
        The created private tool
        """
        self.setdefault(key, PrivateToolArray()).append(
            PrivateTool(type, name, **properties)
        )
        return self[key][-1]

    def setPublicTool(
        self, key: str, type: str, name: str, **properties
    ) -> "PublicTool":
        """Set the named property to a public tool

        Parameters
        ----------
        key : str
            The property on the parent Component to set
        type : str
            The type of the public tool
        name : str
            The name of the new public tool
        **properties
            Any other properties to be set on the public tool

        Returns
        -------
        The created public tool
        """
        self[key] = PublicTool(type, key, **properties)
        return self[key]

    def appendToPublicToolArray(
        self, key: str, type: str, name: str, **properties
    ) -> "PublicTool":
        """Append a new public tool to the named property

        Parameters
        ----------
        key : str
            The property on the parent Component to set
        type : str
            The type of the public tool
        name : str
            The name of the new public tool
        **properties
            Any other properties to be set on the public tool

        Returns
        -------
        The created public tool
        """
        self.setdefault(key, PublicToolArray()).append(
            PublicTool(type, name, **properties)
        )
        return self[key][-1]

    def setService(
        self, key: Optional[str], type: str, name: str, **properties
    ) -> "Service":
        """Set the named property to a service

        Parameters
        ----------
        key : Optional[str]
            The property on the parent Component to set. If None add the service to the extra
            components list
        type : str
            The type of the service
        name : str
            The name of the new service
        **properties
            Any other properties to be set on the service

        Returns
        -------
        The created service
        """
        self[key] = Service(type, key, **properties)
        return self[key]

    def appendToServiceArray(
        self, key: str, type: str, name: str, **properties
    ) -> "Service":
        """Append a new service to the named property

        Parameters
        ----------
        key : str
            The property on the parent Component to set
        type : str
            The type of the service
        name : str
            The name of the new service
        **properties
            Any other properties to be set on the service

        Returns
        -------
        The created service
        """
        self.setdefault(key, ServiceArray()).append(Service(type, name, **properties))
        return self[key][-1]

    def _copy_props(self, other: "Component"):
        """Copy the properties from this to another component"""
        for k, v in self.items():
            if isinstance(v, (Component, ComponentArray)):
                other[k] = v.copy()
            else:
                other[k] = v

    @abc.abstractmethod
    def _create_component(self, sequence: Union[AlgSequence, str], accumulator, flags):
        """Create the underlying component

        Parameters
        ----------
        sequence : Union[AlgSequence, str]
            If running in AnalysisBase, the AlgSequence object must be provided as a hook for the
            public components. If running in athena in the ComponentAccumulator mode a sequence
            name can be provided and the algorithms will be placed in that sequence.
        accumulator : ComponentAccumulator, optional
            If running in athena, a ComponentAccumulator-based configuration can be used instead.
            To activate this mode pass the accumulator the fragment should be merged into here.
            When running in this mode the ConfigFlags should also be provided
        flags : AthConfigFlags, optional
            The ConfigFlags instance when running in the ComponentAccumulator mode
        """

    def _set_props(
        self, component, sequence: Union[AlgSequence, str], accumulator, flags
    ):
        """Set properties on the created component"""
        if accumulator is not None:
            for key, item in self.items():
                if isinstance(item, (Component, ComponentArray)):
                    setattr(component, key, item.get(sequence, accumulator, flags))
                else:
                    setattr(component, key, item)
        else:
            for key, item in self.items():
                if isinstance(item, Component):
                    if item.is_public:
                        # In order to correctly handle conflicts public objects are created
                        # separately and just the type and name are set here
                        setattr(component, key, item.type_and_name)
                    else:
                        # Private tools have to set themselves on the parent as part of their
                        # _create_component method
                        item.get(sequence, accumulator, flags)
                elif isinstance(item, ComponentArray):
                    if item.is_public:
                        setattr(component, key, [x.type_and_name for x in item])
                    else:
                        item.get(sequence, accumulator, flags)
                else:
                    setattr(component, key, item)


class Algorithm(Component):
    """Base class representing an algorithm"""

    def __init__(self, type: str, name: str, **properties):
        super().__init__(
            type=type,
            name=name,
            component_type=Component.ComponentType.Algorithm,
            **properties,
        )

    def copy(self) -> "Algorithm":
        alg = Algorithm(type=self.type, name=self.name)
        self._copy_props(alg)
        return alg

    def _create_component(self, sequence: Union[AlgSequence, str], accumulator, flags):
        if accumulator is not None:
            comp = CompFactory.getComp(self.type)(self.name)
            accumulator.addEventAlgo(comp, sequence)
            return comp
        else:
            return createAlgorithm(self.type, self.name)


class PublicTool(Component):
    """Base class representing a public tool"""

    def __init__(self, type: str, name: str, **properties):
        super().__init__(
            type=type,
            name=name,
            component_type=Component.ComponentType.PublicTool,
            **properties,
        )

    @property
    def full_name(self) -> str:
        return f"ToolSvc.{self.name}"

    def copy(self) -> "PublicTool":
        tool = PublicTool(type=self.type, name=self.name)
        self._copy_props(tool)
        return tool

    def _create_component(self, sequence: Union[AlgSequence, str], accumulator, flags):
        if accumulator is not None:
            comp = CompFactory.getComp(self.type)(self.name)
            accumulator.setPublicTool(comp)
            return comp
        else:
            comp = createPublicTool(self.type, self.name)
            if not is_gaudi:
                sequence += comp
            return comp


class PrivateTool(Component):
    """Base class representing a private tool

    Note that the parent must be set before the actual tool object is created
    """

    def __init__(
        self,
        type: str,
        name: str,
        **properties,
    ):
        """Create the private tool

        Parameters
        ----------
        type: str
            The type of tool to create
        name: str
            The name of the tool
        **properties
            Any properties to be set immediately
        """
        super().__init__(
            type=type,
            name=name,
            component_type=Component.ComponentType.PrivateTool,
            **properties,
        )
        self._parent = None
        self._key = None
        self._in_array = False

    @property
    def full_name(self) -> str:
        if is_gaudi or not self._in_array:
            return super().full_name
        else:
            idx = self._parent.index(self)
            return f"{super().full_name}@{idx}"

    def set_parent(self, parent: Component, key: str):
        """Set the parent of this tool

        Parameters
        ----------
        parent : Component
            The direct parent of this tool
        key : str
            The key this tool has in its parent
        """
        if self._parent is not None:
            raise ValueError("Cannot change the parent of a private tool!")
        self._parent = parent
        self._key = key

    def copy(self) -> "PrivateTool":
        tool = PrivateTool(type=self.type, name=self.name)
        self._copy_props(tool)
        return tool

    def _create_component(self, sequence: Union[AlgSequence, str], accumulator, flags):
        if accumulator is not None:
            return CompFactory.getComp(self.type)(self.name)
        elif is_gaudi:
            comp = CompFactory.getComp(self.type)(self.name)
            if self._in_array:
                getattr(self._parent.get(), self._key).append(comp)
            else:
                setattr(self._parent.get(), comp, self._key)
            return comp
        else:
            parent = self._parent
            key = self._key
            while not parent.is_public:
                key = parent.key + "." + key
                parent = parent._parent
            if self._in_array:
                return parent.get().addPrivateToolInArray(key, self.type)
            else:
                return parent.get().addPrivateTool(key, self.type)


class Service(Component):
    """Base class representing a service"""

    def __init__(self, type: str, name: str, **properties):
        super().__init__(
            type=type,
            name=name,
            component_type=Component.ComponentType.Service,
            **properties,
        )

    def copy(self) -> "Service":
        svc = Service(type=self.type, name=self.name)
        self._copy_props(svc)
        return svc

    def _create_component(self, sequence: Union[AlgSequence, str], accumulator, flags):
        if accumulator is not None:
            comp = CompFactory.getComp(self.type)(self.name)
            accumulator.addService(comp)
            return comp
        else:
            return createService(self.type, self.name, sequence)


class ComponentArray(MutableSequence):
    """Array of a specific component type"""

    def __init__(self, component_type: Component.ComponentType):
        self._component_type = component_type
        self._components = []

    @property
    def component_type(self) -> Component.ComponentType:
        """The component type in this array"""
        return self._component_type

    @property
    def is_public(self) -> bool:
        """Whether the components in this array are public"""
        return self._component_type.is_public

    def __getitem__(self, index: int) -> Component:
        return self._components[index]

    def __setitem__(self, index: int, value: Component):
        if value.component_type != self.component_type:
            raise ValueError(
                f"Cannot add a {value.component_type} into an array of {self.component_type}s"
            )
        self._components[index] = value

    def __delitem__(self, index: int):
        del self._components[index]

    def __len__(self) -> int:
        return len(self._components)

    def insert(self, index: int, value: Component):
        if value.component_type != self.component_type:
            raise ValueError(
                f"Cannot add a {value.component_type} into an array of {self.component_type}s"
            )
        self._components.insert(index, value)

    def subcomponents(self) -> Iterable["Component"]:
        """Iterate over all subcomponents"""
        for component in self:
            yield component
            yield from component.subcomponents()

    @abc.abstractmethod
    def copy(self) -> "ComponentArray":
        """Create a copy of this component array"""

    def get(
        self, sequence: Union[AlgSequence, str] = None, accumulator=None, flags=None
    ):
        """Get the real underlying component, creating it if it hasn't been already

        The first time this is called will lock the component, preventing further modification

        Parameters
        ----------
        sequence : Union[AlgSequence, str]
            If running in AnalysisBase, the AlgSequence object must be provided as a hook for the
            public components. If running in athena in the ComponentAccumulator mode a sequence
            name can be provided and the algorithms will be placed in that sequence. In this case
            it must already exist in the accumulator.
        accumulator : ComponentAccumulator, optional
            If running in athena, a ComponentAccumulator-based configuration can be used instead.
            To activate this mode pass the accumulator the fragment should be merged into here.
            When running in this mode the ConfigFlags should also be provided
        flags : AthConfigFlags, optional
            The ConfigFlags instance when running in the ComponentAccumulator mode
        """
        return [item.get(sequence, accumulator, flags) for item in self]


class PublicToolArray(ComponentArray):
    """Facade class representing an array of public tools"""

    def __init__(self):
        super().__init__(Component.ComponentType.PublicTool)

    def copy(self) -> "PublicToolArray":
        array = PublicToolArray()
        array += [comp.copy() for comp in self]
        return array


class PrivateToolArray(ComponentArray):
    """Facade class representing an array of private tools"""

    def __init__(self):
        super().__init__(Component.ComponentType.PrivateTool)
        self._parent = None
        self._key = None

    def set_parent(self, parent: Component, key: str):
        """Set the parent of this array

        This must be called before any tools are added to it

        Parameters
        ----------
        parent : Component
            The direct parent of this array
        key : str
            The key this array has in its parent
        """
        if self._parent is not None:
            raise ValueError("Cannot change the parent of a private tool!")
        self._parent = parent
        self._key = key
        for tool in self:
            tool.set_parent(parent, key)

    def __setitem__(self, index: int, value: PrivateTool):
        super().__setitem__(index, value)
        value.set_parent(self._parent, self._key)
        value._in_array = True

    def insert(self, index: int, value: Component):
        super().insert(index, value)
        value.set_parent(self._parent, self._key)
        value._in_array = True

    def copy(self) -> "PrivateToolArray":
        array = PrivateToolArray()
        array += [comp.copy() for comp in self]
        return array


class ServiceArray(ComponentArray):
    """Facade class representing an array of services"""

    def __init__(self):
        super().__init__(Component.ComponentType.Service)

    def copy(self) -> "ServiceArray":
        array = ServiceArray()
        array += [comp.copy() for comp in self]
        return array


class ConfigFragment(abc.ABC):
    """Represents a piece of the configured job which can be converted to a series of algorithms"""

    @abc.abstractmethod
    def public_components(self) -> Dict[str, Component]:
        """A mapping of component name -> component for all public components

        NB: This is not used when processing ComponentAccumulators
        """

    @abc.abstractmethod
    def create(
        self, sequence: Union[AlgSequence, str] = None, accumulator=None, flags=None
    ):
        """Creates the full configuration that it describes

        Parameters
        ----------
        sequence : Union[AlgSequence, str]
            If running in AnalysisBase, the AlgSequence object must be provided as a hook for the
            public components. If running in athena in the ComponentAccumulator mode a sequence
            name can be provided and the algorithms will be placed in that sequence. In this case
            it must already exist in the accumulator.
        accumulator : ComponentAccumulator, optional
            If running in athena, a ComponentAccumulator-based configuration can be used instead.
            To activate this mode pass the accumulator the fragment should be merged into here.
            When running in this mode the ConfigFlags should also be provided
        flags : AthConfigFlags, optional
            The ConfigFlags instance when running in the ComponentAccumulator mode

        Returns
        -------
        The list of algorithms that have been created
        """


class AlgorithmList(ConfigFragment, ComponentArray):
    """Piece of the configuration representing a list of algorithms"""

    def __init__(self, algorithms: List[Algorithm] = []):
        super().__init__(Component.ComponentType.Algorithm)
        self += algorithms

    def public_components(self) -> Dict[str, Component]:
        public = {}
        for component in self.subcomponents():
            if component.is_public:
                try:
                    existing = public[component.full_name]
                except KeyError:
                    public[component.full_name] = component
                else:
                    existing.merge(component)
        return public

    def create(
        self, sequence: Union[AlgSequence, str] = None, accumulator=None, flags=None
    ):
        return self.get(sequence, accumulator, flags)

    def copy(self) -> "AlgorithmList":
        array = AlgorithmList()
        array += [comp.copy() for comp in self]
        return array


class JobConfiguration(ConfigFragment, MutableSequence):
    """Represents the whole configured job

    Acts as a list of smaller configuration fragments
    """

    def __init__(self):
        self._fragments: List[ConfigFragment] = []

    def __getitem__(self, index: int) -> ConfigFragment:
        return self._fragments[index]

    def __setitem__(self, index: int, value: ConfigFragment):
        self._fragments[index] = value

    def __delitem__(self, index: int):
        del self._fragments[index]

    def __len__(self) -> int:
        return len(self._fragments)

    def insert(self, index: int, value: Component):
        self._fragments.insert(index, value)

    def __iadd__(self, other):
        """Append a component to this"""
        if isinstance(other, JobConfiguration):
            for x in other:
                self += x
        elif isinstance(other, ConfigFragment):
            self.append(other)
        elif isinstance(other, Algorithm):
            self.append(AlgorithmList([other]))
        else:
            # Otherwise assume that this is a list of algorithms or fragments
            for x in other:
                self += x
        return self

    def public_components(self) -> Dict[str, Component]:
        public = {}
        for fragment in self:
            for name, component in fragment.public_components.items():
                try:
                    existing = public[name]
                except KeyError:
                    public[name] = component
                else:
                    existing.merge(component)
        return public

    def create(
        self, sequence: Union[AlgSequence, str] = None, accumulator=None, flags=None
    ):
        if accumulator is not None:
            if not is_gaudi:
                raise ValueError(
                    "Cannot run in ComponentAccumulator mode in AnalysisBase"
                )
            if flags is None:
                raise ValueError(
                    "The ConfigFlags must be provided in ComponentAccumulator mode"
                )
            if sequence is None:
                sequence = "AthAlgSeq"
            accumulator.addService(CompFactory.CP.SystematicsSvc("SystematicsSvc"))
        else:
            createService("CP::SystematicsSvc", "SystematicsSvc", sequence)
            # First collect the public objects and create them
            for component in self.public_components().values():
                component.get(sequence, accumulator, flags)
        # Now create the fragments
        return sum(
            (fragment.create(sequence, accumulator, flags) for fragment in self), []
        )


class PreconfiguredAlgList(ConfigFragment):
    """Configuration fragment representing a list of already configured algorithms"""

    def __init__(self, algorithms):
        """Create the fragment

        Parameters
        ----------
        algorithms
            The already configured algorithms
        """
        self._algorithms = algorithms

    def public_components(self) -> Dict[str, Component]:
        return {}

    def create(
        self, sequence: Union[AlgSequence, str] = None, accumulator=None, flags=None
    ):
        if accumulator is not None:
            raise ValueError("Cannot convert preconfigured algorithms to CA format")
        else:
            return self._algorithms


class PreconfiguredComponentAccumulator(ConfigFragment):
    """Configuration fragment representing an already configured CA"""

    def __init__(self, accumulator):
        if not is_gaudi:
            raise ValueError("Cannot use CAs in AnalysisBase")
        self._ca = accumulator

    def public_components(self) -> Dict[str, Component]:
        """Unused for component accumulators"""
        return {}

    def create(
        self, sequence: Union[AlgSequence, str] = None, accumulator=None, flags=None
    ):
        # In either version we need to extract the algorithms from our internal CA
        algos = self._ca.getEventAlgos()
        # Remove them from the CA so they aren't used
        self._ca._algorithms = {}
        self._ca._allSequences = []
        if accumulator is not None:
            accumulator.merge(self._ca)
            # Now we have to add in the algorithms. The edge case is if any algorithm is a sequence
            # this needs to be handled differently
            def add_algo(algo, parent_name: str):
                if isinstance(algo, CompFactory.AthSequencer):
                    # For some reason we aren't allowed to add a sequence with members, so strip
                    # those out first
                    algs = algo.Members
                    algo.Members = []
                    accumulator.addSequence(algo, parent_name)
                    for alg in algs:
                        add_algo(alg, algo.name)
                else:
                    accumulator.addEventAlgo(algo, parent_name)

            for alg in algos:
                add_algo(alg, sequence)
        else:
            appendCAtoAthena(self._ca)
        return algos
