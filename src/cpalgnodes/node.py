"""Implementation of the main Node class

A node represents the smallest unit of the job configuration that the scheduler is allowed to
reorder.
"""

import abc
from collections import defaultdict
from typing import Dict, List, Mapping, Optional, Set, Tuple, Union

import boolean

from cpalgnodes.booleanalgebra import algebra
from cpalgnodes.componentfacade import Algorithm, ConfigFragment
from cpalgnodes.containerinfo import ContainerInfo
from cpalgnodes.graph import BaseNode

Dependency = Tuple[str, Optional[str]]
"""Helper class to define data dependencies

The first element of the tuple is the container (nick)name and the second is the auxitem or None
if the dependency is on the container alone.
"""


class Node(BaseNode):
    """Node class implementation that will be used by the default scheduler

    Data dependencies are represented by (container, aux) 2-tuples, where aux is None if the
    dependency is on the container alone.
    Algorithms declare their dependencies through the produces_aux, produces_containers and
    requires_aux properties. The produces_container property also provides information about
    relationships between containers (e.g. where one container is a shallow copy of another). The
    scheduler will use this information to make sure that aux items declared on the parent container
    can be available on the child container.

    Containers are referred to by nicknames, rather than their names in StoreGate. For example,
    nodes will report affecting the 'electrons' container and the scheduler will tell each node the
    correct name (e.g. Electrons) when building the sequence. Where a container will not be
    replaced (e.g. 'EventInfo') this can be used as the nickname.

    Nodes also provide information about preselections which can be applied to the containers that
    they operate on. This information is used by the scheduler to ensure that algoriths operate on
    as few objects as possible.
    """

    #: Special value used to indicate a required input needs all objects in the container
    ALL_OBJECTS = algebra.TRUE

    #: Special value used to indicate the scheduler has complete freedom to set the preselection on a container
    PRESELECTED_OBJECTS = algebra.FALSE

    @abc.abstractproperty
    def produces_aux(self) -> Dict[str, Set[str]]:
        """The auxdata produced by this node, split by container

        Returns
        -------
        A dictionary mapping container nicknames to the set of auxnames the node produces
        """

    @abc.abstractproperty
    def produces_containers(self) -> Dict[str, Optional[str]]:
        """Any containers created by this node

        Containers are returned as a dictionary mapping from the created container nickname to its
        parent container (or None otherwise). A container has a parent if it is a deep copy,
        shallow copy or view. Where a node updates a container it should have itself as the parent.
        """

    @abc.abstractproperty
    def requires_aux(self) -> Dict[str, Set[str]]:
        """The auxdata required by this node, split by container

        Returns
        -------
        A dictionary mapping container nicknames to the set of auxnames the node requires
        """

    @property
    def produces(self) -> Set[Dependency]:
        # We need to flatten the container -> aux mapping to a list of container, aux pairs
        # Also make sure to add if the algorithm just produces the container
        return {
            (container, None)
            for container, parent in self.produces_containers.items()
            if container != parent
        } | {
            (container, aux)
            for container, auxitems in self.produces_aux.items()
            for aux in auxitems
            if aux
        }

    @property
    def requires_containers(self) -> Set[str]:
        """A set of all containers used by the node"""
        requires = set(self.produces_aux) | set(self.requires_aux)
        # Make sure we don't list the container as one of the required containers given
        # that we in fact produce it
        for container, parent in self.produces_containers.items():
            if parent is not None:
                requires.add(parent)
            if container != parent:
                try:
                    requires.remove(container)
                except KeyError:
                    pass
        return requires

    @property
    def requires(self) -> Set[Dependency]:
        # We need to flatten the container -> aux mapping to a list of container, aux pairs
        return {(container, None) for container in self.requires_containers} | {
            (container, aux)
            for container, auxitems in self.requires_aux.items()
            for aux in auxitems
            if aux
        }

    def requires_objects(
        self, required_output: Dict[Dependency, boolean.Expression]
    ) -> Dict[Dependency, boolean.Expression]:
        """The input objects that this algorithm needs

        Part of the preselection mechanism. This method allows each node to tell the scheduler
        which objects upstream nodes need to have run for this node to produce the data required by
        downstream node.

        For example, in the most simple case (which this default implementation satisfies) the node
        calculates object-by-object so the node only needs to see the objects on which its output
        is required.

        Parameters
        ----------
        required_output : Dict[Dependency, boolean.Expression]
            The selections required downstream of this node on its outputs

        Returns
        -------
        The selections this node requires on its inputs
        """

        by_container = defaultdict(lambda: Node.PRESELECTED_OBJECTS)
        for (cont, _), presel in required_output.items():
            by_container[cont] |= presel
        return {(cont, aux): by_container[cont] for cont, aux in self.requires}

    @property
    def has_job_output(self) -> bool:
        """Whether or not this node is responsible for producing output from the job.

        If a node produces output (e.g. a TTree or histogram) it is always required and so the
        scheduler will add a dependency from it to the output node.
        """

    def _on_add(self, scheduler):
        """Method called when the node is added to the scheduler"""

    def _on_remove(self, scheduler):
        """Method called when the node is removed from the scheduler"""

    @abc.abstractmethod
    def create(
        self, container_info: Mapping[str, ContainerInfo]
    ) -> Union[Algorithm, List[Algorithm], ConfigFragment]:
        """Create the algorithm(s) represented by this node

        Parameters
        ----------
        container_info : Mapping[str, ContainerInfo]
            The ContainerInfo for each input/output container

        Returns
        -------
        Union[Algorithm, List[Algorithm], ConfigFragment]
            The configured algorithm(s)
        """
