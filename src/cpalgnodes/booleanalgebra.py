""" Provide a specialisation of the classes from the boolean.py package

This is used mainly to parse boolean expressions. The choices for the operators here are to match
C++ style boolean expressions rather than boolean.py's defaults (which match python expressions)
"""

import copy
from typing import Set

import boolean


class AND(boolean.AND):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.operator = " && "


class OR(boolean.OR):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.operator = " || "


class NOT(boolean.NOT):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.operator = "!"


class TRUE(boolean.boolean._TRUE):
    def __str__(self):
        return "true"


class FALSE(boolean.boolean._FALSE):
    def __str__(self):
        return "false"


class Algebra(boolean.BooleanAlgebra):
    def __init__(self):
        super().__init__(
            TRUE_class=TRUE,
            FALSE_class=FALSE,
            NOT_class=NOT,
            AND_class=AND,
            OR_class=OR,
            # Allow the sort of names we tend to have
            allowed_in_token=(".", "_", ",", "%"),
        )

    def tokenize(self, expr):
        if isinstance(expr, str):
            # Convert from C++-style representation to boolean.py representation
            expr = expr.replace("&&", "&").replace("||", "|")
        return super().tokenize(expr)

    def remove_symbols(self, expr: boolean.Expression, symbols: Set[boolean.Symbol]):
        """Remove symbols from an expression, giving a new expression that is the minimal superset
        of the original expression calculatable without the removed symbols.

        For instance, removing 'Y' from 'X & Y' gives 'X' as 'X' is always true if 'X & Y' is true,
        but removing 'Y' from 'X | Y' gives 'true' as it's impossible to be more specific without
        knowledge of 'Y'

        This is then the OR of the expression with each symbol set to both true and false
        """
        # Remove any that don't exist in the expression
        symbols = copy.copy(symbols)
        symbols &= expr.symbols
        for symbol in symbols:
            expr |= expr.subs({symbol: self.TRUE}) | expr.subs({symbol: self.FALSE})
        return expr.simplify()


algebra = Algebra()

#: Special value used to indicate a required input needs all objects in the container
ALL_OBJECTS = algebra.TRUE
#: Special value used to indicate the scheduler has complete freedom to set the preselection on a container
PRESELECTED_OBJECTS = algebra.FALSE
