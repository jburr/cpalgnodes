"""Helper functions and classes for alg nodes"""

import enum

from PATCore import ParticleDataType

try:
    import Gaudi
except ImportError:
    is_gaudi = False
else:
    is_gaudi = True
    from AthenaConfiguration.AthConfigFlags import AthConfigFlags
    from AthenaConfiguration.AutoConfigFlags import GetFileMD


class DataType(enum.Enum):
    """The different types of data that are run over

    The values of the enum are meant to match those in the ParticleDataType C++ enum
    """

    Data = ParticleDataType.Data
    FullSim = ParticleDataType.Full
    AFII = ParticleDataType.Fast

    @property
    def is_data(self):
        """Is this a data sample"""
        return self == DataType.Data

    @property
    def is_simulation(self):
        """Is this a simulated sample"""
        return not self.is_data


if is_gaudi:

    def data_type_from_flags(flags: AthConfigFlags):
        """Read the data type enum from the job configuration flags"""
        if flags.Input.isMC:
            md = GetFileMD(flags.Input.Files[0])
            try:
                sim_flavour = md["Simulator"]
            except KeyError:
                sim_flavour = md["SimulationFlavour"]
            if sim_flavour.startswith("ATLFAST"):
                return DataType.AFII
            else:
                return DataType.FullSim
        else:
            return DataType.Data


def add_sys_pattern(name):
    """If the provided name doesn't contain the '%SYS%' pattern add it to the end"""
    return name if "%SYS%" in name else name + "_%SYS%"


def replace_sys_pattern(name: str, replace_with: str = None):
    """Replace the %SYS% pattern in the provided string

    In order to deal with the (usual) case where the pattern is added as a '_%SYS%' suffix and the
    pattern to be replaced with is an empty string, this whole suffix will just be removed. In
    order to prevent this, just supply an empty string to the replace_with parameter
    """
    # TODO: Fix this
    if name is None:
        return name
    if replace_with is None and name.endswith("_%SYS%"):
        name = name[: -len("_%SYS%")]
    if replace_with is None:
        replace_with = ""
    return name.replace("%SYS%", replace_with)
