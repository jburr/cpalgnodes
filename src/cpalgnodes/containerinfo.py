from collections import Counter, defaultdict
from typing import Dict, Optional, Union

import boolean

from cpalgnodes.booleanalgebra import ALL_OBJECTS, algebra


class ContainerInfo:
    """Helper class holding information about the current state of a container"""

    def __init__(self):
        #: The name of the container when input to the node
        self.input_name: str = None
        #: The name of the container that the node should output
        self.output_name: str = None
        self._preselection: boolean.Expression = ALL_OBJECTS
        #: The number of counts for each selection defined on the container
        self.selection_counts: Dict[str, Optional[int]] = defaultdict(None)

    @property
    def preselection(self) -> boolean.Expression:
        """The preselection that the node should use"""
        return self._preselection

    @preselection.setter
    def preselection(self, value: Union[str, boolean.Expression]):
        if not isinstance(value, boolean.Expression):
            value = algebra.parse(value)
        self._preselection = value

    def selection_to_property_value(
        self, selection: Union[str, boolean.Expression]
    ) -> str:
        """Convert a selection expression to a string property value

        This will add all of the necessary as_char or as_bool strings
        """
        if not isinstance(selection, boolean.Expression):
            selection = algebra.parse(selection)
        subs = {}
        for symbol in selection.symbols:
            counts = self.selection_counts.get(str(symbol), None)
            subs[symbol] = algebra.Symbol(
                f"{symbol},{'as_char' if counts is None else 'as_bits'}"
            )
        return str(selection.subs(subs))


def mk_temp_name(base_name: str) -> str:
    """Create a unique name for a temporary container

    Parameters
    ----------
    base_name : str
        A base name to use for the container

    Returns
    -------
    str
        The unique name
    """
    count = mk_temp_name._counter[base_name]
    mk_temp_name._counter[base_name] += 1
    return "{}_TMP{}".format(base_name, count)


mk_temp_name._counter = Counter()
