import string
from collections import Counter, defaultdict
from typing import Callable, Dict, Iterable, Mapping, Optional, Set, Union

import boolean

from cpalgnodes.booleanalgebra import PRESELECTED_OBJECTS, algebra
from cpalgnodes.componentfacade import Algorithm
from cpalgnodes.containerinfo import ContainerInfo
from cpalgnodes.node import Dependency, Node


class AlgNode(Algorithm, Node):
    """Node that has one (main) algorithm

    Dictionary access is used to gain access to the algorithm's properties
    """

    def __init__(
        self,
        type: str,
        name: str,
        priority: int = 0,
        inputs: Dict[str, str] = None,
        outputs: Dict[str, str] = None,
        **properties,
    ):
        """Create the algorithm instance

        Parameters
        ----------
        type: str
            The concrete algorithm type to create
        name: str
            The name of the algorithm to create
        priority: int
            The scheduler will try to put nodes with a higher priority first
        inputs: Dict[str, str]
            Mapping from a property on the algorithm to the nickname of the input that will be
            provided to it
        outputs: Dict[str, str]
            Mapping from a property on the algorithm to the nickname of the output that will be
            provided to it
        """
        super().__init__(type=type, name=name, **properties)
        self._priority = priority
        #: Mapping from a property on the algorithm to the nickname of an input it sets
        self._inputs = {}  # Type: Dict[str, str]
        if inputs is not None:
            self._inputs.update(inputs)
        #: Mapping from a property on the algorithm to the nickname of an output it sets
        self._outputs = {}  # Type: Dict[str, str]
        if outputs is not None:
            self._outputs.update(outputs)
        # Map property to (container, selection, is_preselection) tuple
        self._selections = {}  # Type: Dict[str, Tuple[str, boolean.Expression, bool]]

    @classmethod
    def create_unique_name(cls, pattern: str) -> str:
        """Create a unique name for an algorithm

        An example pattern string would be MyAlgNode_{index}

        Parameters
        ----------
        pattern: str
            The format string to use. Should accept an 'index' key
        """
        # Check that the index key is included
        f = string.Formatter()
        found = False
        for _, field_name, _, _ in f.parse(pattern):
            if field_name == "index":
                found = True
            elif field_name is not None:
                raise ValueError(
                    "Input pattern string {} does not have the correct keys".format(
                        pattern
                    )
                )
        if not found:
            pattern += "_{index}"
        # Get a unique index for this algorithm
        index = AlgNode.create_unique_name.counter[pattern]
        AlgNode.create_unique_name.counter[pattern] += 1
        return pattern.format(index=index)

    @property
    def priority(self):
        """The priority of this node - higher priority nodes will be placed first"""
        return self._priority

    @property
    def name(self):
        """The name of this node"""
        return self._name

    def add_input(self, property: str, nickname: str):
        """Add a new input container to the algorithm

        Parameters
        ----------
        property : str
            The name of the property to which the input should be provided
        nickname : str
            The (nick)name of the container which should be provided
        """
        self._inputs[property] = nickname

    def add_output(self, property: str, nickname: str):
        """Add a new output container to the algorithm

        Parameters
        ----------
        property : str
            The name of the property to which the output should be provided
        nickname : str
            The (nick)name of the container which should be provided
        """
        self._outputs[property] = nickname

    def add_selection(
        self,
        property: str,
        selection: Union[str, boolean.Expression],
        input_property: str = None,
        is_preselection: bool = False,
    ):
        """Add a new selection to this algorithm

        Parameters
        ----------
        property : str
            The name of the property to which the selection should be provided
        selection : Union[str, boolean.Expression]
            The selection
        input_property : str, optional
            The name of the property on which the input container is set, by default None. Can be
            omitted if the algorithm has only one input
        is_preselection : bool
            If the property is a preselection. In this case the set value will be or'ed with the
            value provided by the scheduler
        """
        if input_property is None:
            if len(self._inputs) == 1:
                input_property = next(iter(self._inputs))
            else:
                raise KeyError(
                    f"{self} has multiple inputs so an input_property name must be specified"
                )
        if not isinstance(selection, boolean.Expression):
            selection = algebra.parse(selection)
        self._selections[property] = (input_property, selection, is_preselection)

    def input_nickname(self, property: str = None) -> str:
        """Get the container nickname for the specified input to this algorithm

        Parameters
        ----------
        property : str, optional
            The property under which the input will be stored. If there is only one input to this
            algorithm it may be omitted.

        Raises
        ------
        KeyError
            The specified property is not an input or no property is specified where the number of
            inputs does not equal 1.

        Returns
        -------
        str
            The nickname of the container to be supplied to that input property
        """
        if property is None:
            if len(self._inputs) == 1:
                return next(self._inputs.values())
            else:
                raise KeyError(
                    f"{self} has multiple inputs so a property name must be specified"
                )
        else:
            return self._inputs[property]

    def output_nickname(self, property: str = None) -> str:
        """Get the container nickname for the specified output to this algorithm

        Parameters
        ----------
        property : str, optional
            The property under which the output will be stored. If there is only one output to this
            algorithm it may be omitted.

        Raises
        ------
        KeyError
            The specified property is not an output or no property is specified where the number of
            inputs does not equal 1.

        Returns
        -------
        str
            The nickname of the container to be supplied to that output property
        """
        if property is None:
            if len(self._outputs) == 1:
                return next(self._outputs.values())
            else:
                raise KeyError(
                    f"{self} has multiple outputs so a property name must be specified"
                )
        else:
            return self._outputs[property]

    @property
    def produces_containers(self) -> Dict[str, Optional[str]]:
        return {v: None for v in self._outputs.values()}

    @property
    def requires_aux(self) -> Dict[str, Set[str]]:
        requires = {v: {None} for v in self._inputs.values()}
        for input_property, selection in self._selections.values():
            requires[self.input_nickname(input_property)] |= {
                str(symbol) for symbol in selection.symbols
            }
        return requires

    def requires_objects(
        self, required_output: Dict[Dependency, boolean.Expression]
    ) -> Dict[Dependency, boolean.Expression]:
        presels = defaultdict(lambda: PRESELECTED_OBJECTS)
        # Go through the selections we have registered
        for container, selection in self._selections.values():
            presels[container] |= selection
        # Now go through what we produce and see what will be needed
        for cont, aux in self.produces:
            presels[container] |= required_output.get((cont, aux), PRESELECTED_OBJECTS)
        return {
            (container, aux): presels[container] for (container, aux) in self.requires
        }

    def create(self, container_info: Mapping[str, ContainerInfo]) -> Algorithm:
        algorithm = self.copy()
        # Set the input properties on the algorithm
        for prop, nickname in self._inputs.items():
            algorithm[prop] = container_info[nickname].input_name
        # Set the output properties on the algorithm
        for prop, nickname in self._outputs.items():
            algorithm[prop] = container_info[nickname].output_name
        # Set the selection properties on the algorithm
        for prop, (input_prop, selection, is_preselection) in self._selections.items():
            info = container_info.get(self.input_nickname(input_prop), ContainerInfo())
            if is_preselection:
                selection = selection | info.preselection
            algorithm[prop] = info.selection_to_property_value(selection)
        return algorithm


AlgNode.create_unique_name.__func__.counter = Counter()


class SimpleAlgNode(AlgNode):
    """Simple algorithm class to handle most cases

    This class handles the case where there is one obvious 'main' input container and (optionally)
    one main output container. This fulfils the vast majority of use cases for the CP algorithms.
    """

    def __init__(
        self,
        type: str,
        name: str,
        container_property: str,
        container: str,
        container_out_property: str = None,
        container_out: str = None,
        preselection_property: Optional[str] = "preselection",
        preselection: Union[str, boolean.Expression] = PRESELECTED_OBJECTS,
        priority: int = 0,
        requires_aux: Set[
            Union[str, Callable[["SimpleAlgNode"], Union[str, Iterable[str]]]]
        ] = None,
        produces_aux: Set[
            Union[str, Callable[["SimpleAlgNode"], Union[str, Iterable[str]]]]
        ] = None,
        **properties,
    ):
        """Create the algorithm

        Parameters
        ----------
        type: str
            The concrete algorithm type to create
        name: str
            The name of the algorithm to create
        priority: int
            The scheduler will try to put nodes with a higher priority first
        container_property: str
            The name of the property which sets the input container
        container: str
            The (nick)name of the input container
        container_out_property: str
            The name of the property which sets the output container. If not set the algorithm
            does not produce a new output container.
        container_out: str
            The (nick)name of the output container. If not set the algorithm does not produce a
            new output container.
        preselection_property: str
            The name of the property which sets the preselection applied to the input container. If
            set to None, the algorithm will ignore preselections.
        preselection: str, Expression
            The minimal set of objects that this algorithm should see. The algorithm will always
            run over these objects, but if downstream algorithms would use its output on other
            objects it will also run over those.
        priority: int
            The scheduler will try to put nodes with a higher priority first
        requires_aux: Set[str]
            The names of any auxitems (on the single container) required by this node
        produces_aux: Set[str]
            The names of any auxitems (on the single container) that this node produces
        **properties
            Any properties to set directly on the algorithm
        """
        super().__init__(type=type, name=name, priority=priority, **properties)
        self._container_property = container_property
        self._container_out_property = container_out_property
        self._presel_property = preselection_property
        if self._presel_property is not None:
            self.preselection = preselection
        self.add_input(self._container_property, container)
        if self._container_out_property is not None:
            if container_out is None:
                # Assume that if there is an output property but no output container provided we
                # intend to just produce a new temporary container (i.e. update the input nickname
                # container).
                container_out = container
            self.add_output(self._container_out_property, container_out)
        elif container_out is not None:
            raise ValueError(
                f"Output container '{container_out}' provided to an algorithm which has no output container"
            )
        if requires_aux is None:
            requires_aux = set()
        self.__requires_aux = requires_aux
        if produces_aux is None:
            produces_aux = set()
        self.__produces_aux = produces_aux

    @property
    def preselection(self) -> boolean.Expression:
        """The preselection (if any) to this algorithm"""
        if self._presel_property is None:
            return PRESELECTED_OBJECTS
        else:
            return self._selections[self._presel_property][1]

    @preselection.setter
    def preselection(self, value: Union[str, boolean.Expression]):
        self.add_selection(self._presel_property, value, self.input_container, True)

    @property
    def input_container(self):
        """The nickname of the input container"""
        return self.input_nickname(self._container_property)

    @property
    def output_container(self):
        """The nickname of the output container

        Returns None if this node has no output container
        """
        if self._container_out_property is None:
            return None
        else:
            return self.output_nickname(self._container_out_property)

    def _requires_aux(self) -> Set[str]:
        """Return the set of auxdata required on the input container"""
        required = set()
        for aux in self.__requires_aux:
            if callable(aux):
                aux = aux(self)
            if isinstance(aux, str):
                required.add(aux)
            elif aux is not None:
                required |= set(aux)
        return required

    def _produces_aux(self) -> Set[str]:
        """Return the set of auxdata produced on the output container"""
        produced = set()
        for aux in self.__produces_aux:
            if callable(aux):
                aux = aux(self)
            if isinstance(aux, str):
                produced.add(aux)
            elif aux is not None:
                produced |= set(aux)
        return produced

    @property
    def requires_aux(self) -> Dict[str, Set[str]]:
        requires = super().requires_aux
        requires[self.input_container] |= self._requires_aux()
        return requires

    @property
    def produces_aux(self) -> Dict[str, Set[str]]:
        if self._container_out_property is None:
            cont = self.input_container
        else:
            cont = self.output_container
        produces = super().produces_aux
        produces[cont] = {cont: self._produces_aux()}
        return produces

    @property
    def produces_containers(self) -> Dict[str, Optional[str]]:
        produces = super().produces_containers
        if self._container_out_property is not None:
            produces[self.output_container] = self.input_container
        return produces
