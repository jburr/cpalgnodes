"""Algorithm node types and utilities for dealing with selections

A lot of the complexity which has to be dealt with here comes from having to deal with scale
factors. Most scale factors are relatively simple in that unrelated scale factors simply combine
multiplicatively. However there are some cases which complicate things:

- Some selections also have 'inefficiency' scale factors: these are extra weights that must be
  applied whenever the associated selection fails
- Some selections are related and only the tightest scale factor should be applied. For example, if
  applying a Medium muon ID before overlap removal and then a Tight muon ID after, only the tight
  ID should be considered in the overall scale factor

In general, the naive approach to combining similar scale factors (i.e. only take the tightest)
will not apply when there are inefficiency-SFs involved and a dedicated approach must be used.

Because the maths of what we should do in these cases is not settled, if any selection in an
definition provides an inefficiency scale factor, *none* of the other selections may be of the same
type. This should be OK for now as the only place where we have inefficiency scale factors defined
is for jets and there we do not usually apply multiple selections of the same type.
"""


import enum
import logging
from operator import attrgetter, itemgetter
from typing import Iterable, List, Optional, Tuple

from cpalgnodes.algnode import SimpleAlgNode
from cpalgnodes.utils import add_sys_pattern, replace_sys_pattern

log = logging.getLogger(__name__)


class OutOfValidityAction(enum.Enum):
    """How to respond to attempts to calculate values outside of their range of validity"""

    ABORT = 0
    WARNING = 1
    SILENT = 2


class Selection(object):
    """Single selection"""

    def __init__(
        self,
        name: str,
        count: Optional[int] = None,
        previous: Optional[Iterable[str]] = None,
    ):
        """Create the selection

        Parameters
        ----------
        name: str
            The name of the selection
        count: Optional[int]
            The number of bits in the selection, None if the selection is a simple char
        previous: Optional[Iterable[str]]
            Any previous selections that are implicit in this one (used mainly for definitions and OR)
        """
        self._name = name
        self._count = count
        self._previous = () if previous is None else tuple(previous)

    @property
    def name(self) -> str:
        """The name of the selection"""
        return self._name

    @property
    def count(self) -> Optional[int]:
        """The number of bits in the selection, None if the selection is a simple char"""
        return self._count

    @property
    def selection_prop_value(self) -> str:
        """The value to set on a property"""
        return "{},as_{}".format(self.name, "char" if self.count is None else "bits")

    @property
    def previous(self) -> Tuple[str]:
        """Any selections implicit in this"""
        return self._previous


class Definition(Iterable[Selection], Selection):
    """Named selection formed from the AND of multiple sub-selections"""

    def __init__(
        self,
        name: str,
        selections: Iterable[Selection],
    ):
        """Create the selection

        The order of the input selections matters in terms of
        * the order in which the scheduler will try to run them. This implies it should be more
          efficient to put faster selections which cut out as many objects as possible earlier in
          the list. It's also beneficial to put selections which do not, for example, depend on
          the calibration
        * Where anti-SFs exist these will have more impact earlier in the list

        Parameters
        ----------
        name: str
            The name of the selection
        selections : List[Selection]
            The list of selections to be ANDed together
        """
        super().__init__(
            name=name,
            count=None,
            previous=selections,
        )

    @property
    def selections(self):
        """The selections in the definition"""
        return self.previous

    def __iter__(self) -> Iterable[Selection]:
        """Iterate over the subselections"""
        return iter(self.selections)


class ScaleFactor(object):
    """Information about the scale factor for a selection"""

    def __init__(
        self,
        sf: str,
        out_of_valid: Optional[str] = None,
        provides_inefficiency: bool = False,
    ):
        """Create the scale factor

        Parameters
        ----------
        sf : str
            The name of the scale factor
        out_of_valid : Optional[str], optional
            The name of the out of valid decoration (if there is one), by default None
        provides_inefficiency : bool, optional
            Whether the scale factor provides an inefficiency decoration, by default False

        """
        self.sf = sf
        self.out_of_valid = out_of_valid
        self.provides_inefficiency = provides_inefficiency


class SelectionAlgNode(SimpleAlgNode):
    """Base class for nodes which define a single selection on a single container"""

    def __init__(
        self,
        name: str,
        container: str,
        selection_name: str,
        selection_count: int = None,
        priority: int = 10,
        selection_property="selectionDecoration",
        type: str = "CP::AsgSelectionAlg",
        container_property: str = "particles",
        **kwargs,
    ):
        """Create the selection algorithm

        Parameters
        ----------
        name: str
            The name of the algorithm
        container: str
            The nickname of the input container
        selection_name: str
            The name of the selection to set
        selection_count: int
            The number of counts in the selection, None if the selection is a char
        priority:
            The scheduler will try to order higher priority nodes earlier
        selection_property: str
            The name of the property which holds the selection
        type: str
            The type of the C++ algorithm to be created
        container_property: str
            The name of the property which sets the input container
        **kwargs
            Extra keyword arguments which will be passed onto the SimpleAlgNode __init__ method
        """
        super().__init__(
            type=type,
            name=name,
            container_property=container_property,
            container=container,
            priority=priority,
            produces_aux={attrgetter("selection_name")},
            **kwargs,
        )
        self._selection_property = selection_property
        self._selection_count = selection_count
        self.selection_name = selection_name

    @property
    def selection(self) -> Selection:
        """The selection defined by this algorithm"""
        return Selection(
            name=self.selection_name,
            count=self.selection_count,
        )

    @property
    def selection_name(self):
        # remove the ',as_char' or ',as_bits' part of the string
        return self[self._selection_property].partition(",")[0]

    @selection_name.setter
    def selection_name(self, value):
        if "," not in value:
            if self.selection_count is None:
                value += ",as_char"
            else:
                value += ",as_bits"
        self[self._selection_property] = value

    @property
    def selection_count(self):
        """The number of bits in the selection, None if the selection is char"""
        return self._selection_count

    @selection_count.setter
    def selection_count(self, value):
        self._selection_count = value
        # Now update the selection_name based to make sure it has the right
        # as_bits or as_char
        self.selection_name = self.selection_name

    def _on_add(self, scheduler):
        super()._on_add(scheduler)
        scheduler.add_selection(self.input_container, self.selection)

    def _on_remove(self, scheduler):
        scheduler.rm_selection(self.input_container, self.selection.name)
        super()._on_remove(scheduler)


class ScaleFactorAlg(SimpleAlgNode):
    """Base class for algorithms which calculate scale factors"""

    def __init__(
        self,
        type: str,
        name: str,
        container_property: str,
        container: str,
        sf_name: str,
        sf_property: str = "scaleFactorDecoration",
        out_of_validity_deco: str = None,
        out_of_validity_deco_property: str = "outOfValidityDeco",
        out_of_validity_mode: OutOfValidityAction = OutOfValidityAction.SILENT,
        out_of_validity_mode_property: str = "outOfValidity",
        priority: int = -10,
        provides_inefficiency=False,
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        type: str
            The concrete algorithm type to create
        name: str
            The name of the algorithm to create
        container_property: str
            The name of the property which sets the input container
        container: str
            The (nick)name of the input container
        sf_name: str
            The name of the decoration which will hold the scale factor
        sf_property: str
            The name of the property setting the SF decoration on the algorithm
        out_of_validity_deco: str
            The name of the decoration which will hold the scale factor. If not set a sensible
            default will be set
        out_of_validity_deco_property: str
            The name of the property setting the out of validity decoration
        out_of_validity_mode: OutOfValidityAction
            How to react to out of valid range values
        out_of_validity_mode_property:
            The name of the property setting the out of validity mode
        priority: int
            The scheduler will try to put nodes with a higher priority first
        provides_inefficiency: bool
            Whether the algorithm calculates an inefficiency scale factor
        **kwargs
            Any extra kwargs are forwarded to the superclass
        """
        super().__init__(
            type=type,
            name=name,
            container_property=container_property,
            container=container,
            priority=priority,
            produces_aux=(
                kwargs.pop("produces_aux", set())
                | {attrgetter("sf_name"), attrgetter("out_of_validity_deco")}
            ),
            **kwargs,
        )

        self._sf_property = sf_property
        self.sf_name = sf_name
        self._out_of_validity_deco_property = out_of_validity_deco_property
        if out_of_validity_deco is None:
            # Set a sensible default. Remove the %SYS% pattern from the scale factor name
            out_of_validity_deco = "Invalid{}".format(replace_sys_pattern(sf_name))

        self.out_of_validity_deco = out_of_validity_deco

        self._out_of_validity_mode_property = out_of_validity_mode_property
        self.out_of_validity_mode = out_of_validity_mode
        self._provides_inefficiency = provides_inefficiency

    @property
    def sf(self) -> ScaleFactor:
        """Get the scale factor information for this algorithm"""
        return ScaleFactor(
            self.sf_name, self.out_of_validity_deco, self.provides_inefficiency
        )

    @property
    def sf_name(self) -> str:
        """Get the scale factor decoration set by this algorithm"""
        return replace_sys_pattern(self[self._sf_property])

    @sf_name.setter
    def sf_name(self, value: str):
        if value is None:
            self[self._sf_property] = ""
        else:
            self[self._sf_property] = add_sys_pattern(value)

    @property
    def out_of_validity_deco(self) -> str:
        """The name of the out of validity decoration"""
        return self[self._out_of_validity_deco_property]

    @out_of_validity_deco.setter
    def out_of_validity_deco(self, value: str):
        self[self._out_of_validity_deco_property] = value

    @property
    def out_of_validity_mode(self) -> OutOfValidityAction:
        """How to respond to values out of their range of validity"""
        return OutOfValidityAction(self[self._out_of_validity_mode_property])

    @out_of_validity_mode.setter
    def out_of_validity_mode(self, value: OutOfValidityAction):
        self[self._out_of_validity_mode_property] = value.value

    @property
    def provides_inefficiency(self) -> bool:
        """Whether this algorithm calculates an inefficiency SF"""
        return self._provides_inefficiency


class SimpleScaleFactorCombinationAlg(ScaleFactorAlg):
    """Algorithm to combine scale factors for selections which have no inefficiency scale factors"""

    def __init__(
        self,
        container: str,
        selection_name: str,
        input_sfs: List[ScaleFactor],
        sf_name: str = None,
        out_of_valid_name: str = None,
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container : str
            The nickname of the input container
        selection_name : str
            The name of the selection
        input_sels : List[str]
            The list of input scale factors that should be combined
        sf_name : str, optional
            The output scale factor decoration, by default None
        out_of_valid_name : str, optional
            The output out of validity decoration, by default None
        """
        if any(sf.provides_inefficiency for sf in input_sfs):
            log.warning(
                "The simple SF combination algorithm shouldn't be used with SFs with inefficiencies"
            )
        if sf_name is None:
            sf_name = selection_name + "SF"
        super().__init__(
            type="CP::SimpleScaleFactorCombinationAlg",
            name="{}{}ScaleFactorCombinationAlg".format(container, selection_name),
            container_property="particles",
            container=container,
            sf_name=sf_name,
            out_of_validity_deco=out_of_valid_name,
            selection=selection_name,
            inputSFs=[sf.sf for sf in input_sfs],
            inputOutOfValid=[
                sf.out_of_valid for sf in input_sfs if sf.out_of_valid is not None
            ],
            provides_inefficiency=any(sf.provides_inefficiency for sf in input_sfs),
            requires_aux={
                itemgetter("selection"),
                itemgetter("inputSFs"),
                itemgetter("inputOutOfValid"),
            },
            **kwargs,
        )


class InefficiencyScaleFactorCombinationAlg(ScaleFactorAlg):
    """SF combination alg that correctly deals with inefficiency scale factors"""

    def __init__(
        self,
        container: str,
        selection_name: str,
        input_sfs: List[Tuple[Optional[Selection], Optional[ScaleFactor]]],
        sf_name: str = None,
        out_of_valid_name: str = None,
        **kwargs,
    ):
        """Create the algorithm

        Parameters
        ----------
        container : str
            The nickname of the input container
        selection_name : str
            The name of the selection
        input_sfs : List[Tuple[Selection, ScaleFactor]]
            The list of input selections and scale factors that should be combined
        sf_name : str, optional
            The output scale factor decoration, by default None
        out_of_valid_name : str, optional
            The output out of validity decoration, by default None
        """
        if not any(sf.provides_inefficiency for _, sf in input_sfs if sf is not None):
            log.warning(
                "No scale factors have inefficiencies, this is likely an inefficient algorithm"
            )
        if sf_name is None:
            sf_name = selection_name + "SF"
        super().__init__(
            type="CP::InefficiencyScaleFactorCombinationAlg",
            name="{}{}ScaleFactorCombinationAlg".format(container, selection_name),
            container_property="particles",
            container=container,
            sf_name=sf_name,
            out_of_validity_deco=out_of_valid_name,
            inputSelections=[
                sel.name if sel is not None else "true" for sel, _ in input_sfs
            ],
            inputSFs=[
                sf.sf if sf is not None and sf.sf is not None else ""
                for _, sf in input_sfs
            ],
            inputOutOfValid=[
                sf.out_of_valid
                if sf is not None and sf.out_of_valid is not None
                else ""
                for _, sf in input_sfs
            ],
            requires_aux={
                itemgetter("inputSelections"),
                itemgetter("inputSFs"),
                itemgetter("inputOutOfValid"),
            },
            **kwargs,
        )
