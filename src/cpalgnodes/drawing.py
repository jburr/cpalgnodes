"""Helper classes to produce visual representations of the graphs

NB: Right now this is an extremely bare-bones implementation that I need to
make images for the documentation
"""

from collections import defaultdict
from dataclasses import dataclass

import matplotlib.pyplot as plt
import networkx as nx

from cpalgnodes.graph import BaseGraph


@dataclass
class GraphDrawerConfig:
    #: Whether to draw the total order
    draw_total_order: bool = True


def annotate_generations(graph: BaseGraph, attr: str = "Generation"):
    """Annotate the topological generation onto the graph with the specified key"""
    generations = nx.topological_generations(graph.data_dependency_graph)
    values = defaultdict(lambda: 0)
    for i, nodes in enumerate(generations):
        for node in nodes:
            values[node] = i
    nx.set_node_attributes(graph, values, attr)


def multipartite_layout(
    graph: BaseGraph, generation_attr: str = "Generation", **kwargs
):
    """Create a multipartite layout

    Parameters
    ----------
    graph : BaseGraph
        The graph to make the layout for
    generation_attr : str, optional
        The attribute to store the topological generations on, by default "Generation"
    kwargs
        Additional arguments to networkx.multipartite_layout
    """
    annotate_generations(graph, generation_attr)
    return nx.multipartite_layout(graph, generation_attr, **kwargs)


class GraphDrawer:
    """Class to help draw the graph of nodes"""

    def __init__(self, cfg: GraphDrawerConfig = GraphDrawerConfig()):
        """Create the drawer with its configuration"""
        self._cfg = cfg

    def draw_mpl(self, graph: BaseGraph, ax: plt.Axes = None, layout=None):
        """Draw the graph using the matplotlib interface"""

        if ax is None:
            ax = plt.gca()

        if layout is None:
            layout = nx.spring_layout(graph)

        # First draw on the nodes
        nx.draw_networkx_nodes(graph, layout, ax=ax)
        # Label them with their names
        nx.draw_networkx_labels(
            graph, layout, nx.get_node_attributes(graph, "label"), ax=ax
        )

        if self._cfg.draw_total_order:
            nx.draw_networkx_edges(
                graph.filter_edges(lambda _1, _2, attrs: attrs["TotalOrder"]),
                layout,
                ax=ax,
                edge_color="red",
                arrowsize=20,
            )

        data_dependencies = graph.data_dependency_graph
        nx.draw_networkx_edges(data_dependencies, layout, ax=ax, style="dashed")
        nx.draw_networkx_edge_labels(
            data_dependencies,
            layout,
            {
                k: ", ".join(v)
                for k, v in nx.get_edge_attributes(graph, "DataDependencies").items()
            },
            ax=ax,
            rotate=False,
        )
